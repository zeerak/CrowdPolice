package com.az.crowdpolice.essential;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by Zeera on 4/22/2017.
 */

public class App extends Application {

    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
