package com.az.crowdpolice.essential;

import android.content.Context;
import android.content.SharedPreferences;

import com.az.crowdpolice.R;
import com.google.gson.Gson;

/**
 * Created by Administrator on 11/11/2016.
 */

public class SharedPrefManager {

    private SharedPreferences mSharedPrefrence;
    private Context mContext;

    public SharedPrefManager(Context ctx) {
        mContext = ctx;
        mSharedPrefrence = ctx.getSharedPreferences(ctx.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    public void setVal(Object val, String key) {
        SharedPreferences.Editor editor = mSharedPrefrence.edit();
        if (val instanceof String)
            editor.putString(key, (String) val);
        else if (val instanceof Long)
            editor.putLong(key, (Long) val);
        else if (val instanceof Boolean)
            editor.putBoolean(key, (Boolean) val);
        else if (val instanceof Float)
            editor.putFloat(key, (Float) val);
        else if (val instanceof Integer)
            editor.putInt(key, (Integer) val);
        editor.apply();
    }

    public String getStringByKey(String key) {
        if (mSharedPrefrence.contains(key)) {
            return mSharedPrefrence.getString(key, "");
        }
        return "";
    }

    public long getLongByKey(String key) {
        if (mSharedPrefrence.contains(key)) {
            return mSharedPrefrence.getLong(key, 0L);
        }
        return 0L;
    }

    public Boolean getBooleanByKey(String key) {
        if (mSharedPrefrence.contains(key)) {
            return mSharedPrefrence.getBoolean(key, false);
        }
        return null;
    }

    public float getFloatByKey(String key) {
        if (mSharedPrefrence.contains(key)) {
            return mSharedPrefrence.getFloat(key, 0);
        }
        return 0;
    }

    public int getIntegerByKey(String key) {
        if (mSharedPrefrence.contains(key)) {
            return mSharedPrefrence.getInt(key, -1);
        }
        return -1;
    }

    public void saveObjectInSharedPref(Object obj, String key) {
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        setVal(json, key);
    }

    public Object getObjectFromSharedPref(String key,Class toCast) {
        Gson gson = new Gson();
        String json = getStringByKey(key);
        if (json!=null&&!json.equals("")){
            return gson.fromJson(json,toCast);
        }else{
            return null;
        }
    }
}
