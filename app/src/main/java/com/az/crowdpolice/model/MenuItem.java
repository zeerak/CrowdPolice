package com.az.crowdpolice.model;

import android.view.View;

/**
 * Created by Zeera on 3/4/2017.
 */

public class MenuItem {
    private int nameId;
    private int iconIdActivated;
    private int iconIdDeactivated;
    private boolean isSelected;
    private View.OnClickListener listener;

    public MenuItem(int nameId, int iconIdActivated, int iconIdDeactivated, View.OnClickListener listener) {
        this.nameId = nameId;
        this.iconIdActivated = iconIdActivated;
        this.iconIdDeactivated = iconIdDeactivated;
        this.listener = listener;
    }


    public MenuItem(int nameId, View.OnClickListener listener) {
        this.nameId = nameId;
        this.listener = listener;
    }

    public int getNameId(){
        return nameId;
    }

    public int getIconIdActivated(){
        return iconIdActivated;
    }

    public int getIconIdDeactivated(){
        return iconIdDeactivated;
    }

    public void setSelected(boolean isSelected){
        this.isSelected=isSelected;
    }

    public boolean getIsSelected(){
        return isSelected;
    }

    public View.OnClickListener getListener() {
        return listener;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof MenuItem && (((MenuItem) obj).getNameId() == this.getNameId());
    }
}
