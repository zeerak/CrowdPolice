package com.az.crowdpolice.model;

import com.az.crowdpolice.helper.EmojiHelper;
import com.az.crowdpolice.helper.NetworkHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeera on 4/16/2017.
 */

public class MessageModel {

    @SerializedName("ChatId")
    @Expose
    private Integer chatId;
    @SerializedName("SenderUserId")
    @Expose
    private Integer senderUserId;
    @SerializedName("RecieverUserId")
    @Expose
    private Integer recieverUserId;
    @SerializedName("ChatMessage")
    @Expose
    private String chatMessage;
    @SerializedName("SendingTime")
    @Expose
    private String sendingTime;
    @SerializedName("GroupId")
    private Integer groupId;

    private Boolean isRead;
    private Boolean isMe;
    private Boolean isEmoji;

    public MessageModel(Integer chatId, Integer senderUserId, Integer recieverUserId,
                        String chatMessage, String sendingTime, boolean isRead,boolean isGr) {
        this.chatId = chatId;
        this.senderUserId = senderUserId;
        if(!isGr)
             this.recieverUserId = recieverUserId;
        else
            this.groupId = recieverUserId;
        this.chatMessage = NetworkHelper.getDecodedString(chatMessage);
        this.sendingTime = sendingTime;
        this.isRead = isRead;
       // this.isEmoji = EmojiHelper.isEmojiOnly(this.chatMessage);
    }




    public MessageModel(String chatMessage, String sendingTime, boolean isMe) {
        this.chatMessage = chatMessage;
        this.sendingTime = sendingTime;
        this.isMe = isMe;
    }

    public MessageModel(Integer senderUserId, Integer recieverUserId, String chatMessage,boolean isGroup) {
        this.senderUserId = senderUserId;
        if(isGroup)
            this.groupId = recieverUserId;
        else
            this.recieverUserId = recieverUserId;
        this.chatMessage = chatMessage;
    }

    public MessageModel() {
    }

    public String getChatMessage() {
        return chatMessage;
    }

    public String getSendingTime() {
        return sendingTime;
    }

    public boolean isMe() {
        return isMe;
    }

    public void setMe(Boolean me) {
        isMe = me;
    }

    public Integer getChatId() {
        return chatId;
    }

    public Integer getSenderUserId() {
        return senderUserId;
    }

    public Integer getRecieverUserId() {
        return recieverUserId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public void setSendingTime(String sendingTime) {
        this.sendingTime = sendingTime;
    }

    public void setChatId(Integer chatId) {
        this.chatId = chatId;
    }

    public Boolean getEmoji() {
        return isEmoji;
    }
}

