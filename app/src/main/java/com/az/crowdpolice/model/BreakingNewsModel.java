package com.az.crowdpolice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeera on 3/5/2017.
 */

public class BreakingNewsModel {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Code")
    @Expose
    private String code;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Active")
    @Expose
    private Boolean active;
    @SerializedName("EntDate")
    @Expose
    private String entDate;
    @SerializedName("EntHost")
    @Expose
    private String entHost;
    @SerializedName("EntHostIP")
    @Expose
    private String entHostIP;
    @SerializedName("EntUserId")
    @Expose
    private Integer entUserId;
    @SerializedName("EntOperation")
    @Expose
    private String entOperation;
    @SerializedName("EntIMEI")
    @Expose
    private String entIMEI;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getEntDate() {
        return entDate;
    }

    public void setEntDate(String entDate) {
        this.entDate = entDate;
    }

    public String getEntHost() {
        return entHost;
    }

    public void setEntHost(String entHost) {
        this.entHost = entHost;
    }

    public String getEntHostIP() {
        return entHostIP;
    }

    public void setEntHostIP(String entHostIP) {
        this.entHostIP = entHostIP;
    }

    public Integer getEntUserId() {
        return entUserId;
    }

    public void setEntUserId(Integer entUserId) {
        this.entUserId = entUserId;
    }

    public String getEntOperation() {
        return entOperation;
    }

    public void setEntOperation(String entOperation) {
        this.entOperation = entOperation;
    }

    public String getEntIMEI() {
        return entIMEI;
    }

    public void setEntIMEI(String entIMEI) {
        this.entIMEI = entIMEI;
    }

    public String getNews(){
        return "<b>"+name+"</b> : " + description;
    }
}
