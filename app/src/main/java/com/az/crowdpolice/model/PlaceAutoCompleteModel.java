package com.az.crowdpolice.model;


import java.util.List;

/**
 * Created by Administrator on 10/21/2016.
 */

public class PlaceAutoCompleteModel{


    public String placeId;
    public String primaryLocation;
    public String secondaryLocation;
    public String date;
    public boolean isToShow;

    public PlaceAutoCompleteModel(){

    }


    public PlaceAutoCompleteModel(CharSequence placeId, CharSequence primaryLocation, CharSequence secondaryLocation) {
        this.placeId = placeId.toString();
        this.primaryLocation = primaryLocation.toString();
        this.secondaryLocation = secondaryLocation.toString();
    }

    @Override
    public String toString() {
        return primaryLocation.toString();
    }

}
