package com.az.crowdpolice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Zeera on 4/2/2017.
 */

public class WantedModel implements Serializable{

    @SerializedName("Id")
    @Expose
    public Integer id;
    @SerializedName("Code")
    @Expose
    public String code;
    @SerializedName("FirstName")
    @Expose
    public String firstName;
    @SerializedName("MiddleName")
    @Expose
    public String middleName;
    @SerializedName("LastName")
    @Expose
    public String lastName;
    @SerializedName("Age")
    @Expose
    public Integer age;
    @SerializedName("Height")
    @Expose
    public Double height;
    @SerializedName("Weight")
    @Expose
    public Integer weight;
    @SerializedName("WantedFor")
    @Expose
    public String wantedFor;
    @SerializedName("WantedImageURL")
    @Expose
    public String wantedImageURL;
    @SerializedName("CrimeCategoryId")
    @Expose
    public Integer crimeCategoryId;
    @SerializedName("CrimeCategory")
    @Expose
    public String crimeCategory;
    @SerializedName("Active")
    @Expose
    public Boolean active;
    @SerializedName("IsCaptured")
    @Expose
    public Boolean isCaptured;
    @SerializedName("EntDate")
    @Expose
    public String entDate;
    @SerializedName("EntHost")
    @Expose
    public String entHost;
    @SerializedName("EntHostIP")
    @Expose
    public String entHostIP;
    @SerializedName("EntUserId")
    @Expose
    public Integer entUserId;

    public String getCriminalInfo(){
        return lastName+", "+firstName+" ("+wantedFor+")";
    }

    public String getCompeleteName(){
        return lastName+" "+middleName+" " +firstName;
    }

    public String getAge(){
        return "Age "+age +" years";
    }

    public String getHeigth(){
        return height+" feet";
    }

    public String getWeigth(){
        return weight +" KGs";
    }

}
