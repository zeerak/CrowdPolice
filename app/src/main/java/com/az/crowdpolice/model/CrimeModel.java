package com.az.crowdpolice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Zeera on 4/11/2017.
 */

public class CrimeModel implements Serializable {
    @SerializedName("Id")
    @Expose
    public Integer id;
    @SerializedName("CrimeTitle")
    @Expose
    public String crimeTitle;
    @SerializedName("CrimeTypeId")
    @Expose
    public Integer crimeTypeId;
    @SerializedName("CrimeDescription")
    @Expose
    public String crimeDescription;
    @SerializedName("SuspectDescription")
    @Expose
    public String suspectDescription;
    @SerializedName("VehicleDescription")
    @Expose
    public String vehicleDescription;
    @SerializedName("CrimeType")
    @Expose
    public String crimeType;
    @SerializedName("CrimeImageURL")
    @Expose
    public String crimeImageURL;
    @SerializedName("CrimeFileName")
    @Expose
    public String crimeFileName;
    @SerializedName("CrimeFileURL")
    @Expose
    public String crimeFileURL;
    @SerializedName("CrimeLocation")
    @Expose
    public String crimeLocation;
    @SerializedName("UserGPSLatitude")
    @Expose
    public String userGPSLatitude;
    @SerializedName("UserGPSLongitude")
    @Expose
    public String userGPSLongitude;
    @SerializedName("CrimeGPSLatitude")
    @Expose
    public String crimeGPSLatitude;
    @SerializedName("CrimeGPSLongitude")
    @Expose
    public String crimeGPSLongitude;
    @SerializedName("Witness")
    @Expose
    public String witness;
    @SerializedName("EntDate")
    @Expose
    public String entDate;
    @SerializedName("EntHost")
    @Expose
    public String entHost;
    @SerializedName("EntHostIP")
    @Expose
    public String entHostIP;
    @SerializedName("EntOperation")
    @Expose
    public String entOperation;
    @SerializedName("EntUserId")
    @Expose
    public Integer entUserId;
    @SerializedName("EntIMEI")
    @Expose
    public String entIMEI;
    @SerializedName("Active")
    @Expose
    public Boolean isActive;

    public Double getCrimeGPSLatitude() {
        if(crimeGPSLatitude.equals(""))
            return 0.0;
        return Double.valueOf(crimeGPSLatitude);
    }

    public Double getCrimeGPSLongitude() {
        if(crimeGPSLongitude.equals(""))
            return 0.0;
        return Double.valueOf(crimeGPSLongitude);
    }
}
