package com.az.crowdpolice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Zeera on 4/16/2017.
 */

public class UserModel implements Serializable{

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("UserImage")
    @Expose
    private String userImage;
    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("EntHost")
    @Expose
    private String entHost;
    @SerializedName("EntHostIP")
    @Expose
    private String entHostIP;
    private String lastMesssage;
    private String date;

    public Integer getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getLocation() {
        return location;
    }

    public String getMobile() {
        return mobile;
    }

    public int getUserId() {
        return Integer.parseInt(userId);
    }

    public String getUserName() {
        return userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getEntHost() {
        return entHost;
    }

    public String getEntHostIP() {
        return entHostIP;
    }

    public UserModel(int id,String fullName,  String userImage, String lastMesssage, String date) {
        this.fullName = fullName;
        this.id = id;
        this.userImage = userImage;
        this.lastMesssage = lastMesssage;
        this.date = date;
    }
}
