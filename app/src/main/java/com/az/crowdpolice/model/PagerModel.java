package com.az.crowdpolice.model;

import android.support.v4.app.Fragment;

import com.az.crowdpolice.fragments.BaseFragment;

/**
 * Created by Zeera on 3/5/2017.
 */

public class PagerModel {
    private String fragmentName;
    private BaseFragment fragment;

    public PagerModel(String fragmentName, BaseFragment fragment) {
        this.fragmentName = fragmentName;
        this.fragment = fragment;
    }

    public String getFragmentName() {
        return fragmentName;
    }

    public BaseFragment getFragment() {
        return fragment;
    }
}
