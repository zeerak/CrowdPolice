package com.az.crowdpolice.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.crowdpolice.BaseActivity;
import com.az.crowdpolice.R;
import com.az.crowdpolice.callbacks.ITaggingBack;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class LostFoundItem extends BaseFragment implements ITaggingBack{


    public LostFoundItem() {
        // Required empty public constructor
    }
    @OnClick(R.id.btn_lostFound_addItem)
    public void toAddLostFound(View view){
        ((BaseActivity)getActivity()).replaceFragment(new ViewLostItems(),true);
    }
    @OnClick(R.id.btn_lostFound_viewItem)
    public void toLostFound(View view){
        ((BaseActivity)getActivity()).replaceFragment(new ViewLostItems(),true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lost_found_item, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this,view);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public String getName() {
        return "";
    }


    @Override
    public String actionBarActionName() {
        return "";
    }

    @Override
    public int actionBarImage() {
        return 0;
    }

    @Override
    public void performAction() {

    }
}
