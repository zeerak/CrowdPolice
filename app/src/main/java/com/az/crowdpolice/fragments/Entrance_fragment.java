package com.az.crowdpolice.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.az.crowdpolice.BaseActivity;
import com.az.crowdpolice.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class Entrance_fragment extends BaseFragment {


    public Entrance_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_entrance_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
    }


    @OnClick(R.id.iv_emergency)
    public void onEmergncyClick(){
        ((BaseActivity) getActivity()).replaceFragment(new Emergency(),true);
    }

    @OnClick(R.id.iv_ice)
    public void onICEclick(){
        ((BaseActivity) getActivity()).replaceFragment(new ICE_fragment(),true);
    }

    @OnClick(R.id.iv_safe)
    public void onSafeClick(){
        /*((BaseActivity)getActivity()).replaceFragment(new Status(),true);*/
        Toast.makeText(getContext(),"I am safe",Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.iv_sos)
    public void onSosClick(){
        ((BaseActivity)getActivity()).replaceFragment(new SosFragment(),true);
    }

    @Override
    public String getName() {
        return "";
    }

}
