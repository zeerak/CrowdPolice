package com.az.crowdpolice.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.crowdpolice.R;
import com.az.crowdpolice.adapter.EmergencyAdapter;
import com.az.crowdpolice.callbacks.ITaggingBack;
import com.az.crowdpolice.model.EmergencyNumber;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class Emergency extends BaseFragment implements ITaggingBack{
    @BindView(R.id.rv_numbers)
    RecyclerView mRvNumbers;

    public Emergency() {
        // Required empty public constructor
    }

    public static Emergency newInstance() {
        Emergency fragment = new Emergency();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_emergency, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        setAdapter();
    }

    private void setAdapter(){
        EmergencyAdapter adapter = new EmergencyAdapter(getData(),getContext());
        mRvNumbers.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvNumbers.setAdapter(adapter);
    }

    private ArrayList<EmergencyNumber> getData(){
        ArrayList<EmergencyNumber> numbers = new ArrayList<>();
        numbers.add(new EmergencyNumber("Police","10111"));
        numbers.add(new EmergencyNumber("Fire","033333333"));
        numbers.add(new EmergencyNumber("Hospital","10177"));
        numbers.add(new EmergencyNumber("Child Service","033333333"));
        numbers.add(new EmergencyNumber("Gender Violence","033333333"));
        numbers.add(new EmergencyNumber("Ambulance","10177"));
        return numbers;
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public String actionBarActionName() {
        return "";
    }

    @Override
    public int actionBarImage() {
        return 0;
    }

    @Override
    public void performAction() {

    }
}
