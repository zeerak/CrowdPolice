package com.az.crowdpolice.fragments;


import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.crowdpolice.R;
import com.az.crowdpolice.bussiness.ViewPagerBusiness;
import com.az.crowdpolice.callbacks.ILocationUpdated;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrimeTipsHostFragment extends BaseFragment implements ILocationUpdated{

    @BindView(R.id.tl_main_tabs)
    TabLayout mTabCrime;
    @BindView(R.id.vp_main)
    ViewPager mVpCrimes;
    ViewPagerBusiness vpHelper;


    public CrimeTipsHostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crime_tips_host, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        setViewpager();
    }

    private void setViewpager(){

        vpHelper = new ViewPagerBusiness(mTabCrime,mVpCrimes,getContext(),
                getChildFragmentManager(), ViewPagerBusiness.TYPE_PAGER.CRIME);
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onUpdate(Location location) {
        if(vpHelper!=null&&vpHelper.getmCurrentFragment()!=null
                &&vpHelper.getmCurrentFragment() instanceof ILocationUpdated)
            ((ILocationUpdated) vpHelper.getmCurrentFragment()).onUpdate(location);
    }
}
