package com.az.crowdpolice.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.crowdpolice.BaseActivity;
import com.az.crowdpolice.R;
import com.az.crowdpolice.adapter.WantedAdapter;
import com.az.crowdpolice.callbacks.INetworkCallBack;
import com.az.crowdpolice.callbacks.ITaggingBack;
import com.az.crowdpolice.model.WantedModel;
import com.az.crowdpolice.network.ApiImplementor;
import com.az.crowdpolice.network.ApiResponceParser;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class Wanted extends BaseFragment implements ITaggingBack {

    @BindView(R.id.rv_wanted)
    RecyclerView mRvWanted;

    public static Wanted newInstance() {

        Bundle args = new Bundle();

        Wanted fragment = new Wanted();
        fragment.setArguments(args);
        return fragment;
    }

    public Wanted() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wanted, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        getWanted();
    }

    private void getWanted() {
        ApiImplementor.getInstance().getWanted(new INetworkCallBack() {
            @Override
            public void onSuccess(String responce) {
                try {
                    setAdapter(ApiResponceParser.getInstance().getWanted(responce));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setAdapter(ArrayList<WantedModel> mCriminals) {
        WantedAdapter adapter = new WantedAdapter(mCriminals, getContext(),this);
        mRvWanted.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvWanted.setAdapter(adapter);
    }

    public void onItemClicked(WantedModel model){
        ((BaseActivity) getActivity()).replaceFragment(CrimeDetailsFragment.newInstance(model),true);
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public String actionBarActionName() {
        return "";
    }

    @Override
    public int actionBarImage() {
        return 0;
    }

    @Override
    public void performAction() {

    }
}
