package com.az.crowdpolice.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by Zeera on 3/5/2017.
 */

public abstract class BaseFragment extends Fragment {
    public abstract String getName();
    public  boolean performBackFragment(){
        return false;
    }
}
