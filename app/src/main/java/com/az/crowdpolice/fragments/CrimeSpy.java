package com.az.crowdpolice.fragments;


import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.az.crowdpolice.LocationActivity;
import com.az.crowdpolice.MainActivity;
import com.az.crowdpolice.R;
import com.az.crowdpolice.bussiness.SessionClass;
import com.az.crowdpolice.callbacks.ILocationUpdated;
import com.az.crowdpolice.callbacks.INetworkCallBack;
import com.az.crowdpolice.callbacks.ITaggingBack;
import com.az.crowdpolice.model.CrimeCategories;
import com.az.crowdpolice.model.CrimeModel;
import com.az.crowdpolice.model.PlaceObject;
import com.az.crowdpolice.network.ApiImplementor;
import com.az.crowdpolice.network.ApiResponceParser;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrimeSpy extends BaseFragment implements ITaggingBack,ILocationUpdated {

    @BindView(R.id.spinner_ct_categories)
    Spinner mSpCategories;
    @BindView(R.id.et_ct_crime_info)
    EditText mEtCrimeInfo;
    @BindView(R.id.tv_ct_crime_address)
    TextView mtvCrimeAddress;
    @BindView(R.id.et_ct_suspect_desc)
    EditText mEtSuspectDesc;
    @BindView(R.id.et_ct_witness)
    EditText mEtWitnessWhere;
    @BindView(R.id.et_ct_vehicle)
    EditText mEtVehicle;
    @BindView(R.id.et_ct_crime_desc)
    EditText mEtCrimeDesc;

    private final int REQUEST_PLACE = 100;
    private CrimeModel mCrimeModel;

    public CrimeSpy() {
    }

    public static CrimeSpy newInstance() {

        Bundle args = new Bundle();

        CrimeSpy fragment = new CrimeSpy();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crime_spy, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        mCrimeModel = new CrimeModel();
        getLocation();
        ApiImplementor.getInstance().getCrimesCategories(new INetworkCallBack() {
            @Override
            public void onSuccess(String responce) {
                try {
                  setSpinner(  ApiResponceParser.getInstance().getCrimeCategories(responce));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setSpinner(ArrayList<CrimeCategories> categories){
        ArrayAdapter<CrimeCategories> adapter = new ArrayAdapter<>(getActivity(), R.layout.row_spinner,
                R.id.text1, categories);
        adapter.setDropDownViewResource(R.layout.row_spinner_dropdown);
        mSpCategories.setAdapter(adapter);
        mSpCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setCrimeToPost(){
        mCrimeModel.crimeTitle = mEtCrimeInfo.getText().toString();
        mCrimeModel.crimeDescription = mEtCrimeDesc.getText().toString();
        mCrimeModel.suspectDescription = mEtSuspectDesc.getText().toString();
        mCrimeModel.vehicleDescription = mEtVehicle.getText().toString();
        mCrimeModel.witness = mEtWitnessWhere.getText().toString();
        mCrimeModel.crimeTypeId = ((CrimeCategories) mSpCategories.getSelectedItem()).getId();
        mCrimeModel.entUserId = SessionClass.getInstance().getUserId();
        mCrimeModel.isActive = true;
    }

    @OnClick(R.id.btn_ct_Submit)
    public void postCrime(){
        setCrimeToPost();
        ApiImplementor.getInstance().postCrime(mCrimeModel, new INetworkCallBack() {
            @Override
            public void onSuccess(String responce) {
                Toast.makeText(getContext(),"Crime Posted",Toast.LENGTH_LONG).show();
            }
        });
    }


    private void getLocation(){
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).requestLocation();
        }
    }

    @OnClick(R.id.tv_ct_crime_address)
    public void toLocationSelect(){
        Intent i = new Intent(getActivity(), LocationActivity.class);
        startActivityForResult(i,REQUEST_PLACE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            switch (requestCode){
                case REQUEST_PLACE:
                    PlaceObject place = data.getExtras().getParcelable(LocationActivity.PLACE_KEY);
                    if(place!=null){
                        mtvCrimeAddress.setText(place.getAddress());
                        mCrimeModel.crimeLocation = place.getAddress();
                        mCrimeModel.crimeGPSLatitude = String.valueOf(place.getLat());
                        mCrimeModel.crimeGPSLongitude = String.valueOf(place.getLng());
                    }
                    break;
            }
        }
    }

    @Override
    public void onUpdate(Location location) {
        mCrimeModel.userGPSLatitude = String.valueOf(location.getLatitude());
        mCrimeModel.userGPSLongitude = String.valueOf(location.getLongitude());
    }

    @Override
    public String getName() {
        return "";
    }


    @Override
    public String actionBarActionName() {
        return "";
    }

    @Override
    public int actionBarImage() {
        return 0;
    }

    @Override
    public void performAction() {

    }
}
