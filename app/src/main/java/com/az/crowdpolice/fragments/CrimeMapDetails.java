package com.az.crowdpolice.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.az.crowdpolice.R;
import com.az.crowdpolice.callbacks.ITaggingBack;
import com.az.crowdpolice.model.CrimeModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrimeMapDetails extends BaseFragment implements ITaggingBack {
    private static String  ARG_CRIME ="crime";
    private CrimeModel mCurrentCrime;

    @BindView(R.id.tv_crimeTitle)
    TextView mTvCrimeTitle;
    @BindView(R.id.tv_suspect_desc)
    TextView mTvSuspectDesc;
    @BindView(R.id.tv_crime_address)
    TextView mTvCrimeLocation;
    @BindView(R.id.tv_vehicle_desc)
    TextView mTvVehicleDesc;
    @BindView(R.id.tv_witness_details)
    TextView mTvWitnessDesc;

    public CrimeMapDetails() {
        // Required empty public constructor
    }

    public static CrimeMapDetails newInstance(CrimeModel crime) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_CRIME,crime);
        CrimeMapDetails fragment = new CrimeMapDetails();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCurrentCrime = (CrimeModel) getArguments().getSerializable(ARG_CRIME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crime_map_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        setViews();
    }

    private void setViews(){
        mTvCrimeTitle.setText(mCurrentCrime.crimeTitle);
        mTvCrimeLocation.setText(mCurrentCrime.crimeLocation);
        mTvSuspectDesc.setText(mCurrentCrime.suspectDescription);
        mTvVehicleDesc.setText(mCurrentCrime.vehicleDescription);
        mTvWitnessDesc.setText(mCurrentCrime.witness);
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public String actionBarActionName() {
        return "";
    }

    @Override
    public int actionBarImage() {
        return 0;
    }

    @Override
    public void performAction() {

    }
}
