package com.az.crowdpolice.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.az.crowdpolice.R;
import com.az.crowdpolice.adapter.ChatUserAdapter;
import com.az.crowdpolice.adapter.GroupChatAdapter;
import com.az.crowdpolice.bussiness.SessionClass;
import com.az.crowdpolice.callbacks.IMessageReceived;
import com.az.crowdpolice.callbacks.INetworkCallBack;
import com.az.crowdpolice.model.GroupModel;
import com.az.crowdpolice.model.UserModel;
import com.az.crowdpolice.network.ApiImplementor;
import com.az.crowdpolice.network.ApiResponceParser;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatListFragment extends BaseFragment implements IMessageReceived {

    public static final int TYPE_USER = 1;
    public static final int TYPE_GROUP = 2;
    private static final String ARG_TYPE = "type";
    private int mType = -1;
    private Unbinder mUnBinder;


    @BindView(R.id.rvChats)
    RecyclerView mRvChats;


    public ChatListFragment() {
        // Required empty public constructor
    }

    public static ChatListFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt(ARG_TYPE,type);
        ChatListFragment fragment = new ChatListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null&&getArguments().containsKey(ARG_TYPE))
            mType = getArguments().getInt(ARG_TYPE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUnBinder = ButterKnife.bind(this,view);
        getData();
    }

    private void getData(){
        switch (mType){
            case TYPE_USER:
                setChats();
                break;
            case TYPE_GROUP:
                setGroups();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnBinder.unbind();
    }

    private void setChats(){
        ApiImplementor.getInstance().getAllUsers(new INetworkCallBack() {
            @Override
            public void onSuccess(String responce) {
                try {
                    ArrayList<UserModel> users = ApiResponceParser.getInstance().getUsers(responce);
                    for (UserModel user : users) {
                        if(user.getId()== SessionClass.getInstance().getUserId()){
                            users.remove(user);
                            break;
                        }
                    }
                    setAdapter(new ChatUserAdapter(users,getContext()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setGroups(){
        ApiImplementor.getInstance().getAllGroups(new INetworkCallBack() {
            @Override
            public void onSuccess(String responce) {
                try {
                    ArrayList<GroupModel> groups = ApiResponceParser.getInstance().getGroups(responce);
                    setAdapter(new GroupChatAdapter(groups,getContext()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setAdapter(RecyclerView.Adapter adapter){
        if(mRvChats!=null){
            mRvChats.setLayoutManager(new LinearLayoutManager(getContext()));
            mRvChats.setAdapter(adapter);
        }
    }

    @Override
    public String getName() {
        return "";
    }



    @Override
    public void onMessageReceived(Integer userId) {

    }

    @Override
    public void onGroupMessageReceived(Integer groupId) {

    }
}
