package com.az.crowdpolice.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.crowdpolice.R;
import com.az.crowdpolice.adapter.ViewPagerAdapter;
import com.az.crowdpolice.callbacks.ITaggingDrawer;
import com.az.crowdpolice.model.PagerModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChatRootFragment extends BaseFragment implements ITaggingDrawer {

    private Unbinder mUnBinder;
    @BindView(R.id.tb_chat)
    TabLayout mtbChats;
    @BindView(R.id.vp_chats)
    ViewPager mVpChats;


    public ChatRootFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat_root, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUnBinder = ButterKnife.bind(this,view);
        setChatsViewPager();
    }

    public void setChatsViewPager(){
        ArrayList<PagerModel> frags = new ArrayList<>();
        frags.add(new PagerModel("Users",ChatListFragment.newInstance(ChatListFragment.TYPE_USER)));
        frags.add(new PagerModel("Groups",ChatListFragment.newInstance(ChatListFragment.TYPE_GROUP)));
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager(),frags);
        mVpChats.setAdapter(adapter);
        mtbChats.setupWithViewPager(mVpChats);

    }

    @Override
    public String getName() {
        return "";
    }



}
