package com.az.crowdpolice.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.az.crowdpolice.R;
import com.az.crowdpolice.adapter.ChatAdapter;
import com.az.crowdpolice.bussiness.SessionClass;
import com.az.crowdpolice.callbacks.IKeyBoardOpen;
import com.az.crowdpolice.callbacks.IMessageReceived;
import com.az.crowdpolice.callbacks.INetworkCallBack;
import com.az.crowdpolice.callbacks.ITaggingBack;
import com.az.crowdpolice.database.DataBasehadlerClass;
import com.az.crowdpolice.helper.DateHelper;
import com.az.crowdpolice.helper.LogHelper;
import com.az.crowdpolice.helper.NetworkHelper;
import com.az.crowdpolice.model.GroupModel;
import com.az.crowdpolice.model.MessageModel;
import com.az.crowdpolice.model.UserModel;
import com.az.crowdpolice.network.ApiImplementor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends BaseFragment implements ITaggingBack,
        IMessageReceived,
        IKeyBoardOpen{
    private int senderId = SessionClass.getInstance().getUserId();

    public static final int TYPE_ONE_TO_ONE = 1200;
    public static final int TYPE_GROUP = 1201;

    private static final String ARG_USER = "user";
    private static final String ARG_GROUP = "group";
    private static final String ARG_TYPE = "type";

    private Unbinder mUnBinder;
    @BindView(R.id.rv_chat_messages)
    RecyclerView mRvChats;
    @BindView(R.id.et_chat_message)
    EditText mEtMessage;

    LinearLayoutManager mLlManager;
    ChatAdapter mAdapter;
    private ArrayList<MessageModel> mMessages;
    private UserModel mReceiver;
    private GroupModel mGroup;
    private int mType;
    private boolean isGroup;
    private DataBasehadlerClass mDbHelper;
    private int mLastChatId;

    public static ChatFragment newInstance(UserModel user, int type) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_USER, user);
        args.putInt(ARG_TYPE, type);
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }
    public static ChatFragment newInstance(GroupModel group, int type) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_GROUP, group);
        args.putInt(ARG_TYPE, type);
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mType = getArguments().getInt(ARG_TYPE);
            if (getArguments().containsKey(ARG_USER)) {
                mReceiver = ((UserModel) getArguments().getSerializable(ARG_USER));
            }else if(getArguments().containsKey(ARG_GROUP)){
                mGroup = ((GroupModel) getArguments().getSerializable(ARG_GROUP));
            }
        }
        isGroup = mType==TYPE_GROUP;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUnBinder = ButterKnife.bind(this, view);
        mDbHelper = SessionClass.getInstance().getmDbHelper(getActivity());
        initializeAdapter();
        getMessageFromDb();
    }

    private void getMessageFromDb(){
        switch (mType){
            case TYPE_ONE_TO_ONE:
                mMessages = mDbHelper.getAllMessags(mReceiver.getId(),senderId);
                break;
            case TYPE_GROUP:
                mMessages = mDbHelper.getAllGroupMessags(mGroup.getId());
                break;

        }
        mAdapter.setData(mMessages);
        if(mMessages.size()>0)
            mLastChatId = mMessages.get(mMessages.size()-1).getChatId();
        scrollChatToPosition(null);
    }

    private void initializeAdapter() {
        mAdapter = new ChatAdapter(null, getContext());
        mLlManager = new LinearLayoutManager(getContext());
        mRvChats.setLayoutManager(mLlManager);
        mRvChats.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUnBinder != null)
            mUnBinder.unbind();
        mDbHelper = null;
    }

    @OnClick(R.id.iv_btn_send)
    public void sentMessage() {
        String message = mEtMessage.getText().toString();
        if (message.trim().isEmpty())
            return;
        if (mMessages != null && mAdapter != null) {
            mEtMessage.getText().clear();
            final MessageModel messageModel = getMessage(message);
            messageModel.setSendingTime(DateHelper.getCurrentDate());
            messageModel.setMe(true);
            mMessages.add(messageModel);
            mAdapter.notifyItemChanged(mMessages.size() - 1);
            scrollChatToPosition(null);
            ApiImplementor.getInstance().sendMessage(new INetworkCallBack() {
                @Override
                public void onSuccess(String responce) {
                    try {
                        LogHelper.debugLog(responce);
                        JSONObject jRes = new JSONObject(responce);
                        int chatId = jRes.getInt("ChatId");
                        messageModel.setChatId(chatId);
                        messageModel.setRead(true);
                        SessionClass.getInstance().getmDbHelper(getActivity()).addMessage(messageModel,isGroup);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },getMessage(NetworkHelper.getEncodedString(message)),isGroup);
        }
    }

    private MessageModel getMessage(String messageText){
        return new MessageModel(senderId,isGroup?mGroup.getId():mReceiver.getId(),
               messageText,isGroup);
    }

    @Override
    public String actionBarActionName() {
       return "";
    }
    //region action
    @Override
    public int actionBarImage() {
        return 0;
    }

    @Override
    public void performAction() {
    }

    @Override
    public String getName() {
        if(mReceiver!=null){
            return mReceiver.getFullName();
        }else if(mGroup!=null)
            return mGroup.getName();
        return "";
    }

    //endregion

    @Override
    public void onMessageReceived(Integer userId) {
        getMessageFromDb();
        scrollChatToPosition(null
        );
    }

    @Override
    public void onGroupMessageReceived(Integer groupId) {
        getMessageFromDb();
        scrollChatToPosition(null);
    }

    @Override
    public void keyBoardStateChanged(boolean isOpen) {
        if(isOpen)
            scrollChatToPosition(null);
    }

    private void scrollChatToPosition(@Nullable Integer pos){
        if (mLlManager != null) {
            mLlManager.scrollToPosition(pos==null?mMessages.size()-1:pos);
        }

    }
}
