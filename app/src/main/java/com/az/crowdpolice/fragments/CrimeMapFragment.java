package com.az.crowdpolice.fragments;


import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.az.crowdpolice.BaseActivity;
import com.az.crowdpolice.LocationActivity;
import com.az.crowdpolice.MainActivity;
import com.az.crowdpolice.R;
import com.az.crowdpolice.bussiness.SessionClass;
import com.az.crowdpolice.callbacks.ILocationUpdated;
import com.az.crowdpolice.callbacks.INetworkCallBack;
import com.az.crowdpolice.callbacks.ITaggingBack;
import com.az.crowdpolice.model.CrimeModel;
import com.az.crowdpolice.model.PlaceObject;
import com.az.crowdpolice.network.ApiImplementor;
import com.az.crowdpolice.network.ApiResponceParser;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrimeMapFragment extends BaseFragment
        implements ITaggingBack, OnMapReadyCallback,GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener,ILocationUpdated{

    GoogleMap mMap;
    private final int REQUEST_PLACE = 100;
    Unbinder unbinder;
    public static final int DEFAULT_ZOOM = 14;
    private String mCrimeStatistics;
    private int mTotalCrimes;

    @BindView(R.id.tv_Serach)
    TextView mTvLocationName;

    public CrimeMapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crime_map, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this,view);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);

    }

    public void getCrimeFromServer(Double lat,Double lng) {
        ApiImplementor.getInstance().getReportedCrimes(new INetworkCallBack() {
            @Override
            public void onSuccess(String responce) {
                try {
                    addMarkers(ApiResponceParser.getInstance().getCrimes(responce));
                    mCrimeStatistics = ApiResponceParser.getInstance().getStatistic(responce);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },lat,lng);
    }

    public void addMarkers(ArrayList<CrimeModel> crimes) {
        if(crimes.size() == 0)
            Toast.makeText(getContext(),"No Crimes Found In Your Area",Toast.LENGTH_LONG).show();
        mTotalCrimes = crimes.size();
        for (int i = 0; i < crimes.size(); i++) {

            CrimeModel crime = crimes.get(i);
            if (crime.getCrimeGPSLatitude() != 0) {
                LatLng point = new LatLng(crime.getCrimeGPSLatitude(), crime.getCrimeGPSLongitude());
                addMarkerAndInfoWindow(point, crime);
            }
        }
    }


    private void addMarkerAndInfoWindow(LatLng position,
                                        @Nullable CrimeModel crimeModel) {
        Marker m = mMap.addMarker(new MarkerOptions()
                .position(position)
                .icon(BitmapDescriptorFactory.defaultMarker(crimeModel==null?BitmapDescriptorFactory.HUE_GREEN:
                        BitmapDescriptorFactory.HUE_RED)));
        if(crimeModel!=null){
            m.setTag(crimeModel);
            //m.setSnippet(crimeModel.crimeTitle);
            //m.setTitle(crimeModel.crimeTitle);
            //m.showInfoWindow();
        }

    }


    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.clear();
        updatePositionAndGetCrimes();
    }

    public void updatePositionAndGetCrimes(){
        Location location = SessionClass.getInstance().getmCurrentUserLocation();
        if(location!=null){
            getCrimeFromServer(location.getLatitude(),location.getLongitude());
            LatLng userPos = new LatLng(location.getLatitude(),
                    location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userPos,DEFAULT_ZOOM));
            addMarkerAndInfoWindow(userPos,null);
        }else{
            if(getActivity() instanceof MainActivity)
            ((MainActivity) getActivity()).requestLocation();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        ((BaseActivity)getActivity()).replaceFragment(CrimeMapDetails.newInstance((CrimeModel) marker.getTag()),true);
    }

    @OnClick(R.id.tv_Serach)
    public void onSerachClicked(){
        Intent i = new Intent(getActivity(), LocationActivity.class);
        startActivityForResult(i,REQUEST_PLACE);
    }

    @Override
    public void onDestroy() {
        killOldMap();
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        ///killOldMap();
        super.onDetach();
    }

    @Override
    public void onPause() {
        super.onPause();
        killOldMap();
    }

    private void killOldMap() {
        SupportMapFragment mapFragment = ((SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.map_fragment));

        if(mapFragment != null) {
            FragmentManager fM = getChildFragmentManager();
            fM.beginTransaction().remove(mapFragment).commit();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            switch (requestCode){
                case REQUEST_PLACE:
                    PlaceObject place = data.getExtras().getParcelable(LocationActivity.PLACE_KEY);
                    if(place!=null){
                        mTvLocationName.setText(place.getAddress());
                        moveMapToPosition(place.getLat(),place.getLng(),mMap);
                        getCrimeFromServer(place.getLat(),place.getLng());
                    }
                    break;
            }
        }
    }

    private void moveMapToPosition(double lat,double lng,GoogleMap map){
        if (map == null) {
            return;
        }
        LatLng latLng = new LatLng(lat,lng);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,DEFAULT_ZOOM));
    }


    @Override
    public String actionBarActionName() {
        return "Statistics";
    }

    @Override
    public int actionBarImage() {
        return 0;
    }

    @Override
    public void performAction() {
        if(mTotalCrimes>0){
            ((BaseActivity)getActivity()).replaceFragment
                    (CrimeStatisticFragment.newInstance(mCrimeStatistics,mTotalCrimes),true);
        }
    }

    @Override
    public void onUpdate(Location location) {
        updatePositionAndGetCrimes();
    }
}
