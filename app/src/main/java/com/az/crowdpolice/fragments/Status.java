package com.az.crowdpolice.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.az.crowdpolice.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Status extends BaseFragment implements OnMapReadyCallback {
    public static final int STATE_SAFE = 100;
    public static final int STATE_DANGER = 101;
    int mapCurrentState;


    @BindView(R.id.btn_save)
    Button mBtnSave;
    @BindView(R.id.btn_danger)
    Button mBtnDanger;

    public Status() {
        // Required empty public constructor
    }

    public static Status newInstance() {
        Status fragment = new Status();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_status, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        mapCurrentState = STATE_SAFE;
        setMapState(STATE_SAFE);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    @Override
    public String getName() {
        return "";
    }

    @OnClick(R.id.btn_save)
    public void setSave(){
       setMapState(STATE_SAFE);
    }

    @OnClick(R.id.btn_danger)
    public void setDanger(){
        setMapState(STATE_DANGER);
    }

    private void setMapState(int state){
        mapCurrentState = state;
        switch (state){
            case STATE_DANGER:
                mBtnSave.setSelected(false);
                mBtnDanger.setSelected(true);
                mBtnDanger.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
                mBtnSave.setTextColor(ContextCompat.getColor(getContext(),R.color.blue_imsave));
                break;
            case STATE_SAFE:
                mBtnSave.setSelected(true);
                mBtnDanger.setSelected(false);
                mBtnSave.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
                mBtnDanger.setTextColor(ContextCompat.getColor(getContext(),R.color.red_imdanger));
                break;
        }
    }

}
