package com.az.crowdpolice.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.crowdpolice.R;
import com.az.crowdpolice.callbacks.ITaggingBack;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewLostItems extends BaseFragment implements ITaggingBack {


    public ViewLostItems() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_lost, container, false);
    }

    @Override
    public String getName() {
        return "";
    }


    @Override
    public String actionBarActionName() {
        return "";
    }

    @Override
    public int actionBarImage() {
        return 0;
    }

    @Override
    public void performAction() {

    }
}
