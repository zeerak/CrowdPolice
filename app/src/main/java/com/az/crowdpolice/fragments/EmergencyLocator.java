package com.az.crowdpolice.fragments;


import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.az.crowdpolice.MainActivity;
import com.az.crowdpolice.R;
import com.az.crowdpolice.bussiness.SessionClass;
import com.az.crowdpolice.callbacks.ILocationUpdated;
import com.az.crowdpolice.callbacks.ITaggingBack;
import com.az.crowdpolice.helper.LogHelper;
import com.az.crowdpolice.model.CrimeModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmergencyLocator extends BaseFragment implements ITaggingBack, OnMapReadyCallback,
        ILocationUpdated {
    private Unbinder unbinder;
    private GoogleMap mMap;
    private BottomSheetBehavior mSheetBehavior;
    public static final int DEFAULT_ZOOM = 14;

    @BindView(R.id.bs_filters)
    View mBSFilters;
    @BindView(R.id.tv_filters)
    TextView mTvFilters;
    @BindView(R.id.sb_distance)
    SeekBar mSBDistance;
    @BindView(R.id.tv_distance)
    TextView mTvDistance;
    @BindView(R.id.sp_places)
    Spinner mSpGooglePlace;


    public EmergencyLocator() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_emergency_locator, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);
        setBottomSheet();
        setSpinner();
        setSeekBar();
    }

    private void setBottomSheet() {

        mSheetBehavior = BottomSheetBehavior.from(mBSFilters);
        mSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                float moveFactor = (bottomSheet.getHeight() * slideOffset);
                mTvFilters.setAlpha(1 - slideOffset);
            }
        });
    }

    public void updatePosition(){
        Location location = SessionClass.getInstance().getmCurrentUserLocation();
        if(location!=null){
            LatLng userPos = new LatLng(location.getLatitude(),
                    location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userPos,DEFAULT_ZOOM));
            addMarkerAndInfoWindow(userPos,null);
        }else{
            if(getActivity() instanceof MainActivity)
                ((MainActivity) getActivity()).requestLocation();
        }
    }

    private void addMarkerAndInfoWindow(LatLng position,
                                        @Nullable Object object) {
        Marker m = mMap.addMarker(new MarkerOptions()
                .position(position)
                .icon(BitmapDescriptorFactory.defaultMarker(object == null ? BitmapDescriptorFactory.HUE_GREEN :
                        BitmapDescriptorFactory.HUE_RED)));
        if (object != null) {
            m.setTag(object);
        }
    }

    private void setSeekBar(){
        mSBDistance.setMax(50*1000);
        mSBDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                float disInKm = (float)i/1000;
                String distance = String.format(Locale.US,"%.2f KM",disInKm);
                mTvDistance.setText(distance);
                LogHelper.informationLog(i+"");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mSBDistance.setHovered(true);
        mSBDistance.setProgress(5000);
    }

    private void setSpinner(){
        final List<String> spinnerItems=new ArrayList<String>(Arrays.asList(getContext().getResources()
                .getStringArray(R.array.places_google)));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.row_spinner,
                R.id.text1, spinnerItems);
        adapter.setDropDownViewResource(R.layout.row_spinner_dropdown);
        mSpGooglePlace.setAdapter(adapter);
        mSpGooglePlace.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @OnClick(R.id.tv_filters)
    public void showFilterSheet() {
        mSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    @Override
    public String actionBarActionName() {
        return "";
    }

    @Override
    public int actionBarImage() {
        return 0;
    }

    @Override
    public void onUpdate(Location location) {
        updatePosition();
    }

    @Override
    public void performAction() {

    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
}
