package com.az.crowdpolice.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.crowdpolice.R;
import com.az.crowdpolice.callbacks.ITaggingBack;
import com.az.crowdpolice.model.WantedModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrimeDetailsFragment extends BaseFragment implements ITaggingBack{
    private static final String ARG_MODEL ="wanted";

    private WantedModel mCurrentCriminal;

    @BindView(R.id.iv_criminal_image)
    ImageView mIvCriminalImage;
    @BindView(R.id.tv_wanted_Status)
    TextView mTvWantedStatus;
    @BindView(R.id.tv_criminal_name)
    TextView mTvCriminalName;
    @BindView(R.id.tv_criminal_age)
    TextView mTvCriminlaAge;
    @BindView(R.id.tv_criminal_heigth)
    TextView mTvCriminalHeight;
    @BindView(R.id.tv_wanted)
    TextView mTvWantedFor;
    @BindView(R.id.tv_details_name)
    TextView mTvDetailsName;
    @BindView(R.id.tv_details_dob)
    TextView mTvDetailsDob;
    @BindView(R.id.tv_details_height)
    TextView mTvDeatilsHeight;
    @BindView(R.id.tv_details_wanted_For)
    TextView mTvDetailsWWantedFor;
    @BindView(R.id.tv_details_weight)
    TextView mTvdeatilsWeight;

    public static CrimeDetailsFragment newInstance(WantedModel wanted) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_MODEL,wanted);
        CrimeDetailsFragment fragment = new CrimeDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public CrimeDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null)
            mCurrentCriminal = ((WantedModel) getArguments().getSerializable(ARG_MODEL));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crime_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        setViews();
    }

    public void setViews(){
        mTvCriminalName.setText(mCurrentCriminal.getCompeleteName());
        mTvCriminalHeight.setText(mCurrentCriminal.getHeigth());
        mTvCriminlaAge.setText(mCurrentCriminal.getAge());
        mTvDetailsName.setText(String.format("Suspect :%s", mCurrentCriminal.getCompeleteName()));
        mTvDetailsDob.setText(String.format("Date of Birth: Not Mention"));
        mTvDeatilsHeight.setText(String.format("Height: %s", mCurrentCriminal.getHeigth()));
        mTvdeatilsWeight.setText(String.format("Weight: %s", mCurrentCriminal.getWeigth()));
        Picasso.with(getContext()).load(mCurrentCriminal.wantedImageURL)
                .placeholder(R.drawable.user_placeholder)
                .into(mIvCriminalImage);
    }

    @Override
    public String getName() {
        return "";
    }


    @Override
    public String actionBarActionName() {
        return "";
    }

    @Override
    public int actionBarImage() {
        return 0;
    }

    @Override
    public void performAction() {

    }
}
