package com.az.crowdpolice.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.crowdpolice.R;
import com.az.crowdpolice.callbacks.ITaggingBack;
import com.az.crowdpolice.helper.ViewUtitlity;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrimeStatisticFragment extends BaseFragment implements ITaggingBack{

    private PieChart mCrimePieChart;
    private static final String ARG_STATISTICS = "stats";
    private static final String ARG_TOTAL_CRIMES = "total_crimes";
    private String mStatsCrimes;
    private int mTotalCrime;


    public CrimeStatisticFragment() {
        // Required empty public constructor
    }

    public static CrimeStatisticFragment newInstance(String stats,int totalCrimes) {

        Bundle args = new Bundle();
        args.putString(ARG_STATISTICS, stats);
        args.putInt(ARG_TOTAL_CRIMES,totalCrimes);
        CrimeStatisticFragment fragment = new CrimeStatisticFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crime_statistic, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            mStatsCrimes = getArguments().getString(ARG_STATISTICS);
            mTotalCrime = getArguments().getInt(ARG_TOTAL_CRIMES);
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCrimePieChart = (PieChart) view.findViewById(R.id.pc_crimes);
        setPieChart();
    }

    private void setPieChart(){
        List<PieEntry> entries = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(mStatsCrimes);
            Iterator<?> keys = jsonObject.keys();

            while( keys.hasNext() ) {
                String key = (String) keys.next();
                int numbers = jsonObject.getInt(key);
                float per = ((float)numbers/mTotalCrime)*100;
                entries.add(new PieEntry(per, key));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        PieDataSet set = new PieDataSet(entries, "Crime Category");
        set.setColors(ColorTemplate.JOYFUL_COLORS);
        PieData data = new PieData(set);
        set.setSliceSpace(3);
        data.setValueFormatter(new PercentFormatter());
        mCrimePieChart.setDrawEntryLabels(false);
        data.setValueTextSize(14);
        Description description = new Description();
        description.setText("Showing all Crimes percentage in your area");
        mCrimePieChart.setDescription(description);
        mCrimePieChart.setHoleRadius(8);
        mCrimePieChart.setTransparentCircleRadius(11);
        mCrimePieChart.getLegend().setPosition(Legend.LegendPosition.ABOVE_CHART_LEFT);
        mCrimePieChart.getLegend().setOrientation(Legend.LegendOrientation.VERTICAL);
        mCrimePieChart.getLegend().setTextSize(12);
        mCrimePieChart.getDescription().setTextSize(14);
        mCrimePieChart.setData(data);
        mCrimePieChart.invalidate();
    }



    @Override
    public String getName() {
        return "";
    }

    @Override
    public String actionBarActionName() {
        return null;
    }

    @Override
    public int actionBarImage() {
        return 0;
    }

    @Override
    public void performAction() {

    }
}
