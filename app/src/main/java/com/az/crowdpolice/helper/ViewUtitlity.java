package com.az.crowdpolice.helper;

import android.content.Context;

/**
 * Created by Zeera on 5/1/2017.
 */

public class ViewUtitlity {
    public static float pxFromDp(float dp, Context mContext) {
        return dp * mContext.getResources().getDisplayMetrics().density;
    }
}
