package com.az.crowdpolice.helper;

/**
 * Created by Zeera on 4/23/2017.
 */

public class EmojiHelper {
    private final static String emo_regex = "([\\u20a0-\\u32ff\\ud83c\\udc00-\\ud83d\\udeff\\udbb9\\udce5-\\udbb9\\udcee])";

    public static boolean isEmojiOnly(String message){
        for (String word : message.split(" ")) {
            if (word.matches(emo_regex)) {
                System.out.println(word);
            }else{
                return false;
            }
        }
        return true;
    }
}
