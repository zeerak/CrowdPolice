package com.az.crowdpolice.helper;

import android.util.Log;

/**
 * Created by Administrator on 20-Nov-15.
 */
public class LogHelper {

    public static final String LOG_TAG = "Cata_black";

    private static final boolean allowLog = true;

    public static void errorLog(String message) {
        if (allowLog)
            Log.e(LOG_TAG, message);
    }

    public static void errorLogWithException(String message, Exception exception) {
        if (allowLog)
            Log.e(LOG_TAG, message + " " + exception);
    }
    public static void debugLog(String message) {
        if (allowLog)
            Log.d(LOG_TAG, message);
    }
    public static void informationLog(String message) {
        if (allowLog)
            Log.i(LOG_TAG, message);
    }
    public static void verboseLog(String message) {
        if (allowLog)
            Log.v(LOG_TAG, message);
    }
    public static void warnLog(String message) {
        if (allowLog)
            Log.w(LOG_TAG, message);
    }

}
