package com.az.crowdpolice.helper;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;

import com.az.crowdpolice.R;
import com.az.crowdpolice.bussiness.SessionClass;
import com.az.crowdpolice.callbacks.ILocationUpdated;


/**
 * Created by Administrator on 3/9/2017.
 */

public class GpsHelper {

    public static final int REQUEST_GPS = 1001;
    private Context mContext;

    private LocationListener mListner;
    private LocationManager locationManager;
    private AlertDialog gpsDialog;
    boolean isFirstMessage;
    private Location mLocation;

    public GpsHelper(Context context) {
        mContext = context;
        isFirstMessage = true;
    }

    public boolean askForPermmision(@Nullable final ILocationUpdated locationUpdated) {
        if (!PermissionsUtitlty.isLocationPermissionAllowed(mContext)) {
            PermissionsUtitlty.showPermissionsDialogForActivity
                    ((Activity) mContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_GPS);
            return false;
        } else {
            locationManager = (LocationManager) mContext
                    .getSystemService(Context.LOCATION_SERVICE);
            boolean isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (isGPSEnabled) {
                getLocation(locationUpdated);
                return true;
            } else {
                getLocation(locationUpdated);
                buildAlertMessageNoGps(mContext);
                return false;
            }

        }
    }

    private void getLocation(@Nullable final ILocationUpdated locationUpdated) {
        mListner = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                SessionClass.getInstance().setmCurrentUserLocation(location);
                mLocation = location;
                if (gpsDialog != null)
                    gpsDialog.hide();
                if (locationUpdated != null)
                    locationUpdated.onUpdate(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                LogUtility.debugLog("onStatusChanged");
            }

            @Override
            public void onProviderEnabled(String s) {
                LogUtility.debugLog("onProviderEnabled");
            }

            @Override
            public void onProviderDisabled(String s) {
                LogUtility.debugLog("onProviderDisabled");
            }
        };
        try {

            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER,
                    mListner, mContext.getMainLooper());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void buildAlertMessageNoGps(final Context ctx) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        if (gpsDialog == null || !gpsDialog.isShowing())
            gpsDialog = builder.setMessage(isFirstMessage ? R.string.message_nogps_1 : R.string.message_nogps_2)
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            ctx.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                        }
                    }).create();
        gpsDialog.show();
        isFirstMessage = !isFirstMessage;
    }

    @Nullable
    public Location getmLocation() {
        return mLocation;
    }
}