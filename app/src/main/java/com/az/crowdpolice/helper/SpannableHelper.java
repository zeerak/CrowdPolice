package com.az.crowdpolice.helper;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;

/**
 * Created by Administrator on 12/27/2016.
 */

public class SpannableHelper {
    SpannableString mStyledString ;
    int mStart;
    int mEnd;
    int mFlag;

    public SpannableHelper(String baseString, int start, int end){
        mStyledString = new SpannableString(baseString);
        mStart = start;
        mEnd = end;
        mFlag = SpannableString.SPAN_INCLUSIVE_INCLUSIVE;
    }

    public void setmFlag(int flag) {
        this.mFlag = mFlag;
    }

    public SpannableHelper makeBold(){
        mStyledString.setSpan(new StyleSpan(Typeface.BOLD), mStart, mEnd, mFlag);
        return this;
    }

    public SpannableHelper makeBold(int start,int end){
        mStyledString.setSpan(new StyleSpan(Typeface.BOLD), start, end, mFlag);
        return this;
    }

    public SpannableHelper underline(){
        mStyledString.setSpan(new UnderlineSpan(), mStart, mEnd, mFlag);
        return this;
    }

    public SpannableHelper underline(int start,int end){
        mStyledString.setSpan(new UnderlineSpan(), start, end, mFlag);
        return this;
    }

    public SpannableHelper changeTextColor(int color){
        mStyledString.setSpan(new ForegroundColorSpan(color), mStart, mEnd, mFlag);
        return this;
    }

    public SpannableHelper changeTextColor(int color,int start,int end){
        mStyledString.setSpan(new ForegroundColorSpan(color), start, end, mFlag);
        return this;
    }

    public SpannableHelper changeHighLightColor(int color){
        mStyledString.setSpan(new BackgroundColorSpan(color), mStart, mEnd, mFlag);
        return this;
    }

    public SpannableHelper changeHighLightColor(int color,int start,int end){
        mStyledString.setSpan(new BackgroundColorSpan(color), start, end, mFlag);
        return this;
    }

    public SpannableHelper superscript(){
        mStyledString.setSpan(new SuperscriptSpan(), mStart, mEnd, mFlag);
        return this;
    }

    public SpannableHelper superscript(int start,int end){
        mStyledString.setSpan(new SuperscriptSpan(), start, end, mFlag);
        return this;
    }

    public SpannableHelper subscript(){
        mStyledString.setSpan(new SubscriptSpan(), mStart, mEnd, mFlag);
        return this;
    }

    public SpannableHelper subscript(int start,int end){
        mStyledString.setSpan(new SubscriptSpan(), start, end, mFlag);
        return this;
    }

    public SpannableHelper relativeSize(float relativeSize){
        mStyledString.setSpan(new RelativeSizeSpan(relativeSize), mStart, mEnd, mFlag);
        return this;
    }

    public SpannableHelper relativeSize(float relativeSize,int start,int end){
        mStyledString.setSpan(new RelativeSizeSpan(relativeSize), start, end, mFlag);
        return this;
    }


    public SpannableHelper setSpanClickUrl(String url){
        mStyledString.setSpan(new URLSpan(url), mStart, mEnd, mFlag);
        return this;
    }

    public SpannableHelper setSpanClickUrl(String url,int start,int end){
        mStyledString.setSpan(new URLSpan(url), start, end, mFlag);
        return this;
    }


    public SpannableHelper setClickSpan(ClickableSpan onClick){
        mStyledString.setSpan(onClick, mStart, mEnd, mFlag);
        return this;
    }

    public SpannableString getStyledString() {
        return mStyledString;
    }
}
