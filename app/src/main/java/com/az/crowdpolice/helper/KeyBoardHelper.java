package com.az.crowdpolice.helper;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewTreeObserver;

import com.az.crowdpolice.callbacks.IKeyBoardOpen;

/**
 * Created by Zeera on 4/22/2017.
 */

public class KeyBoardHelper {
    boolean isOpened = false;
    public void setListenerToRootView(Activity ctx, final IKeyBoardOpen listener) {
        final View activityRootView = ctx.getWindow().getDecorView().findViewById(android.R.id.content);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.
                OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > 100) { // 99% of the time the height diff will be due to a keyboard.
                    //Toast.makeText(getApplicationContext(), "Gotcha!!! softKeyboardup", Toast.LENGTH_SHORT).show();

                    if (isOpened == false) {
                        listener.keyBoardStateChanged(true);
                       //scrollMyListViewToBottom();
                        //Do two things, make the view top visible and the editText smaller
                    }
                    isOpened = true;
                } else if (isOpened == true) {
                    //Toast.makeText(getApplicationContext(), "softkeyborad Down!!!", Toast.LENGTH_SHORT).show();
                    listener.keyBoardStateChanged(false);
                    isOpened = false;
                }
            }
        });
    }
}
