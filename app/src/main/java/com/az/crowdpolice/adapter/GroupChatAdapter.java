package com.az.crowdpolice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.az.crowdpolice.BaseActivity;
import com.az.crowdpolice.MainActivity;
import com.az.crowdpolice.R;
import com.az.crowdpolice.fragments.ChatFragment;
import com.az.crowdpolice.helper.CircleTransform;
import com.az.crowdpolice.model.GroupModel;
import com.az.crowdpolice.model.UserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 4/17/2017.
 */

public class GroupChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<GroupModel> mData;
    private Context mContext;

    public GroupChatAdapter(ArrayList<GroupModel> mData, Context mContext) {
        this.mData = mData;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat, null);
        GroupChatAdapter.ViewHolder holder = new GroupChatAdapter.ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        GroupModel item = mData.get(position);
        GroupChatAdapter.ViewHolder holder1 = (GroupChatAdapter.ViewHolder) holder;
        holder1.tvUserName.setText(item.getName());
        /*Picasso.with(mContext).load(item.getUserImage()).
                placeholder(R.drawable.profile).
                transform(new CircleTransform()).
                into(holder1.ivUserImage);*/
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.iv_user_image)
        ImageView ivUserImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ((RelativeLayout) tvUserName.getParent()).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((BaseActivity)mContext).replaceFragment(ChatFragment.newInstance(mData.get(getAdapterPosition()),
                            ChatFragment.TYPE_GROUP),true);
                    if (mContext instanceof MainActivity) {
                        ((MainActivity) mContext).getGroupMessages(mData.get(getAdapterPosition()).getId());
                    }
                }
            });
        }
    }
}
