package com.az.crowdpolice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.az.crowdpolice.R;
import com.az.crowdpolice.callbacks.IItemClickListener;
import com.az.crowdpolice.model.PlaceAutoCompleteModel;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 10/21/2016.
 */

public class
LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder>
        implements Filterable {

    private GoogleApiClient mGoogleApiClient;
    private AutocompleteFilter mPlaceFilter;
    private LatLngBounds mBounds;
    private List<PlaceAutoCompleteModel> mResultList;
    private Context mContext;
    private IItemClickListener mListener;
    private Set<String> keys;

    public LocationAdapter(Context context, LatLngBounds bounds,
                           AutocompleteFilter filter) {
        mContext = context;
        mBounds = bounds;
        mResultList = new ArrayList<>();
    }

    public LocationAdapter(List<PlaceAutoCompleteModel> mResultList, Context mContext, IItemClickListener mListener) {
        this.mResultList = mResultList;
        this.mContext = mContext;
        this.mListener = mListener;
    }

    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            mGoogleApiClient = null;
        } else {
            mGoogleApiClient = googleApiClient;
        }
    }

    @Override
    public LocationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_location, null);
        return new LocationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LocationAdapter.ViewHolder holder, int position) {
        PlaceAutoCompleteModel place = mResultList.get(position);
        holder.locationName.setText(place.primaryLocation);
        holder.locationCity.setText(place.secondaryLocation);
    }

    @Override
    public int getItemCount() {
        if(mResultList==null)
            return 0;
        return mResultList.size();
    }

    public PlaceAutoCompleteModel getItem(int position){
        if(mResultList==null)
            return null;
        return mResultList.get(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView locationName, locationCity;

        private ViewHolder(View itemView) {
            super(itemView);
            locationCity = (TextView) itemView.findViewById(R.id.tv_city_name);
            locationName = (TextView) itemView.findViewById(R.id.tv_location_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(mListener!=null)
                mListener.onClick(view,getAdapterPosition());
        }
    }

    private ArrayList<PlaceAutoCompleteModel> getPredictions(CharSequence constraint) {
        if (mGoogleApiClient != null) {
            //LogHelper.informationLog("Executing autocomplete query for: " + constraint);
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                                    mBounds, mPlaceFilter);
            // Wait for predictions, set the timeout.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                //((BaseActivity)mContext).showNoConnectivityDialog();
                //LogHelper.informationLog("Error getting place predictions: " + status
                //        .toString());
                autocompletePredictions.release();
                return null;
            }
           // LogHelper.informationLog("Query completed. Received " + autocompletePredictions.getCount()
             //       + " predictions.");
            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            ArrayList resultList = new ArrayList<>(autocompletePredictions.getCount());

            while (iterator.hasNext()) {
                AutocompletePrediction prediction = iterator.next();
                resultList.add(new PlaceAutoCompleteModel(prediction.getPlaceId(),
                        prediction.getPrimaryText(null),prediction.getSecondaryText(null)));
            }
            // Buffer release
            autocompletePredictions.release();
            return resultList;
        }
        //LogHelper.errorLog("Google API client is not connected.");
        return null;
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null) {
                    // Query the autocomplete API for the entered constraint
                    mResultList = getPredictions(constraint);
                    if (mResultList != null) {
                        // Results
                        results.values = mResultList;
                        results.count = mResultList.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    // The API returned at least one result, update the data.
                    notifyDataSetChanged();
                } else {
                    // The API did not return any results, invalidate the data set.
                    notifyDataSetChanged();
                }
            }
        };
        return filter;
    }

    public void setmListener(IItemClickListener mListener) {
        this.mListener = mListener;
    }
}
