package com.az.crowdpolice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.az.crowdpolice.R;
import com.az.crowdpolice.fragments.BaseFragment;
import com.az.crowdpolice.fragments.Wanted;
import com.az.crowdpolice.model.EmergencyNumber;
import com.az.crowdpolice.model.WantedModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 4/2/2017.
 */

public class WantedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<WantedModel> mData;
    private Context mContext;
    private Wanted fragment;

    public WantedAdapter(ArrayList<WantedModel> mData, Context mContext, Wanted fragment) {
        this.mData = mData;
        this.mContext = mContext;
        this.fragment = fragment;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_wanted, null);
        WantedAdapter.ViewHolder holder = new WantedAdapter.ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        WantedModel item = mData.get(position);
        WantedAdapter.ViewHolder holderVw = ((WantedAdapter.ViewHolder) holder);
        holderVw.tvCriminalDesc.setText(item.getCriminalInfo());
        Picasso.with(mContext).load(item.wantedImageURL)
                .placeholder(R.drawable.user_placeholder)
                .into(holderVw.ivCriminalImaage);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_criminalImage)
        ImageView ivCriminalImaage;
        @BindView(R.id.tv_criminal_decr)
        TextView tvCriminalDesc;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ((RelativeLayout) ivCriminalImaage.getParent()).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.onItemClicked(mData.get(getAdapterPosition()));
                    //Toast.makeText(mContext,mData.get(getAdapterPosition()).getNumber(),Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
