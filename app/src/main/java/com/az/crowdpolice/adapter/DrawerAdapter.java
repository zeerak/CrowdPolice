package com.az.crowdpolice.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;


import com.az.crowdpolice.R;
import com.az.crowdpolice.callbacks.IItemClickListener;
import com.az.crowdpolice.model.MenuItem;
import java.util.ArrayList;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.mViewHolder> {
    private ArrayList<MenuItem> list;
    public Context context;
    private IItemClickListener listener;
    private int mSelectedPos;

    public DrawerAdapter(ArrayList<MenuItem> list, Context context, IItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        mSelectedPos = 1;
    }

    @Override
    public mViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_drawer_adapter, null);
        mViewHolder holder = new mViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(mViewHolder holder, int position) {
        MenuItem item = list.get(position);
        holder.name.setText(context.getResources().getString(item.getNameId()));
        if (mSelectedPos == position) {
            setColorResources(holder, R.color.blue_color_app, R.color.blue_color_app, item.getIconIdActivated());
        } else {
            setColorResources(holder, R.color.black, R.color.grey, item.getIconIdDeactivated());
        }
    }

    private void setColorResources(mViewHolder holder, int textColor, int saperator_Color, int imageResId) {
        holder.divider.setBackgroundColor(ContextCompat.getColor(context, saperator_Color));
        holder.name.setTextColor(ContextCompat.getColor(context, textColor));
        holder.icon.setImageResource(imageResId);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class mViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        View divider;
        ImageView icon;
        TextView name;
        LinearLayout parent;

        mViewHolder(View itemView) {
            super(itemView);


            divider = itemView.findViewById(R.id.divider);
            parent = (LinearLayout) itemView.findViewById(R.id.parent);
            parent.setOnClickListener(this);
            name = (TextView) itemView.findViewById(R.id.name);
            icon = (ImageView) itemView.findViewById(R.id.icon);

        }


        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.parent) {
                listener.onClick(view, getAdapterPosition());
                mSelectedPos = getAdapterPosition();
                notifyDataSetChanged();
            }
        }
    }
}
