package com.az.crowdpolice.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.az.crowdpolice.fragments.BaseFragment;
import com.az.crowdpolice.model.PagerModel;

import java.util.ArrayList;

/**
 * Created by Zeera on 3/5/2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    ArrayList<PagerModel> mFragments;

    public ViewPagerAdapter(FragmentManager fm, ArrayList<PagerModel> mFragments) {
        super(fm);
        this.mFragments = mFragments;
    }

    @Override
    public BaseFragment getItem(int position) {
        Fragment fragment = null;
        return mFragments.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
      return mFragments.get(position).getFragmentName();
    }
}



