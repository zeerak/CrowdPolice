package com.az.crowdpolice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.az.crowdpolice.R;
import com.az.crowdpolice.helper.DateHelper;
import com.az.crowdpolice.helper.NetworkHelper;
import com.az.crowdpolice.model.EmergencyNumber;
import com.az.crowdpolice.model.MessageModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 4/19/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<MessageModel> mData;
    private Context mContext;
    private final int TYPE_SENDER = 1;
    private final int TYPE_RECEIVER = 2;

    public ChatAdapter(ArrayList<MessageModel> mData, Context mContext) {
        this.mData = mData;
        this.mContext = mContext;
    }

    public void setData(ArrayList<MessageModel> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case TYPE_SENDER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sender, null);
               holder = new SenderHolder(v);
                break;
            case TYPE_RECEIVER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_received, null);
                holder = new SenderHolder(v);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MessageModel message = mData.get(position);
        SenderHolder dataHolder = (SenderHolder) holder;
        dataHolder.tvMessage.setText(message.getChatMessage());
        dataHolder.tvDate.setText(DateHelper.getGMTDate(message.getSendingTime()));
        if(message.getEmoji()!=null && message.getEmoji()){
            dataHolder.tvMessage.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        }else{
            dataHolder.tvMessage.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
        }
    }

    @Override
    public int getItemCount() {
        return mData==null?0:mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).isMe() ? TYPE_SENDER : TYPE_RECEIVER;
    }

    class SenderHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtMessage)
        TextView tvMessage;
        @BindView(R.id.tv_date)
        TextView tvDate;

        public SenderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    class ReceiverHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtMessage)
        TextView tvMessage;
        @BindView(R.id.tv_date)
        TextView tvDate;

        public ReceiverHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

}
