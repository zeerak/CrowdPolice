package com.az.crowdpolice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.az.crowdpolice.R;
import com.az.crowdpolice.model.EmergencyNumber;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 4/1/2017.
 */

public class EmergencyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<EmergencyNumber> mData;
    private Context mContext;

    public EmergencyAdapter(ArrayList<EmergencyNumber> mData, Context mContext) {
        this.mData = mData;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_emergency, null);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EmergencyNumber item = mData.get(position);
        ((ViewHolder) holder).tvPhoneId.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_phone_id)
        TextView tvPhoneId;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ((RelativeLayout) tvPhoneId.getParent()).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(mContext,mData.get(getAdapterPosition()).getNumber(),Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
