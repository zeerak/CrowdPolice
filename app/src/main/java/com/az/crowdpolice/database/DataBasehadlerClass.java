package com.az.crowdpolice.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.az.crowdpolice.bussiness.SessionClass;
import com.az.crowdpolice.model.MessageModel;
import com.az.crowdpolice.model.UserModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

/**
 * Created by Zeera on 4/16/2017.
 */

public class DataBasehadlerClass extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "menyooMessenger";

    //  tables name
    private static final String TABLE_RESTAURANT = "restaurant";
    private static final String TABLE_MESSAGES = "messages";
    private static final String TABLE_GROUP_MESSAGES = "group_messages";

    // Restaurant Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_IMG_PATH = "image_path";

    //Message Table Columns Name
    private static final String KEY_SENDER_ID = "senderId";
    private static final String KEY_RECEIVER_ID = "receiverId";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_DATE = "date";
    private static final String KEY_IS_READ = "isRead";
    private static final String KEY_GROUP_ID = "groupId";


    public DataBasehadlerClass(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //exportDatabse(DATABASE_NAME);
    }


    public DataBasehadlerClass(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /***
     * Creating table
     ***/
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createRestaurantTable());
        sqLiteDatabase.execSQL(createMessageTable());
        sqLiteDatabase.execSQL(createGroupMessageTable());

    }

    public String createRestaurantTable() {
        return "CREATE TABLE " + TABLE_RESTAURANT + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_IMG_PATH + " TEXT" + ")";
    }

    public String createMessageTable() {
        return "CREATE TABLE " + TABLE_MESSAGES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_SENDER_ID + " INTEGER," +
                 KEY_RECEIVER_ID + " INTEGER," +
                KEY_MESSAGE + " TEXT," + KEY_DATE + " TEXT,"
                + KEY_IS_READ + " INTEGER" + ")";
    }

    public String createGroupMessageTable() {
        return "CREATE TABLE " + TABLE_GROUP_MESSAGES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_SENDER_ID + " INTEGER,"
                +KEY_GROUP_ID + " INTEGER," +
                KEY_MESSAGE + " TEXT," + KEY_DATE + " TEXT,"
                + KEY_IS_READ + " INTEGER" + ")";
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_RESTAURANT);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP_MESSAGES);
        // Create tables again
        onCreate(sqLiteDatabase);
    }


    //region Restaurants Methods
    // Adding new contact
    public void addRestaurant(UserModel user) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, user.getFullName());
        values.put(KEY_IMG_PATH, user.getUserImage());
        values.put(KEY_ID, user.getId());

        // Inserting Row
        if (!checkRestaurantExists(user.getUserId())) {
            db.insert(TABLE_RESTAURANT, null, values);
            db.close(); // Closing database connection
        }

    }

    private boolean checkRestaurantExists(int userId) {
        String selectQuery = "SELECT  * FROM " + TABLE_RESTAURANT + " WHERE ID" + " = " + userId;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            cursor.close();
            return true;

        }
        cursor.close();
        //db.close();
        return false;
    }


    public ArrayList<UserModel> getAllChats() {
        String query = "Select r.id,r.name,r.image_path, m.message,m.date from restaurant r  join " +
                "messages m on r.[id] = m.resId where m.message IS NOT NULL group by r.id order by m.date desc";
        ArrayList<UserModel> users = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UserModel user = new UserModel(Integer.parseInt(cursor.getString(0))
                        , cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
                // Adding contact to list
                users.add(user);
            } while (cursor.moveToNext());

        }
        cursor.close();
        db.close();
        return users;
    }
    //endregion

    //region Message Methods
    public void addMessage(MessageModel msg,boolean isGr) {
        SQLiteDatabase db = this.getReadableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, msg.getChatId());
        values.put(KEY_SENDER_ID, msg.getSenderUserId());
        if(isGr)
            values.put(KEY_GROUP_ID,msg.getGroupId());
        else
             values.put(KEY_RECEIVER_ID, msg.getRecieverUserId());
        values.put(KEY_MESSAGE, msg.getChatMessage());
        values.put(KEY_DATE, msg.getSendingTime());
        values.put(KEY_IS_READ, msg.isRead());

        // Inserting Row
        db.insert(isGr?TABLE_GROUP_MESSAGES:TABLE_MESSAGES, null, values);
        db.close(); // Closing database connection
    }

    public int updateReadState(int resId, int state) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_IS_READ, state);
        //values.put(KEY_PH_NO, contact.getPhoneNumber());
        String[] param = new String[]{String.valueOf(resId)};
        // updating row
        int i = db.update(TABLE_MESSAGES, values, KEY_RECEIVER_ID + " = ?", param);
        db.close();
        return i;

    }

    public ArrayList<MessageModel> getAllMessags(int receiverId,int senderId) {
        ArrayList<MessageModel> messages = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_MESSAGES + " WHERE " +
                KEY_RECEIVER_ID + "=" + receiverId + " AND "+KEY_SENDER_ID +"=" + senderId
                +" OR " +KEY_RECEIVER_ID+ "=" +senderId +" AND " + KEY_SENDER_ID + " = "+receiverId+
                " order by " + KEY_DATE + " asc";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                MessageModel message = new MessageModel(
                        cursor.getInt(cursor.getColumnIndex(KEY_ID)),
                        cursor.getInt(cursor.getColumnIndex(KEY_SENDER_ID)),
                        cursor.getInt(cursor.getColumnIndex(KEY_RECEIVER_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_MESSAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_DATE)),
                        cursor.getInt(cursor.getColumnIndex(KEY_IS_READ)) == 1,false);
                message.setMe(message.getSenderUserId() == SessionClass.getInstance().getUserId());
                // Adding contact to list
                messages.add(message);
            } while (cursor.moveToNext());
        }

        db.close();
        return messages;
    }

    public ArrayList<MessageModel> getAllGroupMessags(int groupId) {
        ArrayList<MessageModel> messages = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_GROUP_MESSAGES + " WHERE " +
                KEY_GROUP_ID + "=" + groupId +
                " order by " + KEY_DATE + " asc";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                MessageModel message = new MessageModel(
                        cursor.getInt(cursor.getColumnIndex(KEY_ID)),
                        cursor.getInt(cursor.getColumnIndex(KEY_SENDER_ID)),
                        cursor.getInt(cursor.getColumnIndex(KEY_GROUP_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_MESSAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_DATE)),
                        cursor.getInt(cursor.getColumnIndex(KEY_IS_READ)) == 1,true);
                message.setMe(message.getSenderUserId() == SessionClass.getInstance().getUserId());
                // Adding contact to list
                messages.add(message);
            } while (cursor.moveToNext());
        }

        db.close();
        return messages;
    }

    public Integer getLastIdOfChat(boolean isGr){
        StringBuilder query = new StringBuilder("Select ").
                append(KEY_ID).append(" from ").append(isGr?TABLE_GROUP_MESSAGES:TABLE_MESSAGES)
                .append(" order by ").append(KEY_ID).append(" desc ").append(" limit 1");
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query.toString(), null);
        if (cursor.moveToFirst()) {
            do {
                return cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        return null;
    }

    public void deleteMessages() {
        String selectQuery = "DELETE  FROM " + TABLE_MESSAGES;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
        db.close();
    }

    public int getUnReadCount() {
        String query = "Select Count(m.isRead) as unreadCount from messages m left join restaurant r on r.[id] = m.resId where m.isRead = 0";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int unread =0;
        if (cursor.moveToFirst()) {
            unread =  cursor.getInt(0);
        }
        db.close();
        return unread;

    }

    public int isContainsMessage(){
        return 0;
    }

    public int getUnReadCountForRestaurant(int resId) {
        String query = "Select Count(m.isRead) as unreadCount from messages" +
                " m left join restaurant r on r.[id] = m.resId where m.isRead = 0 AND r.[id] = " + resId;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int unread = 0;
        if (cursor.moveToFirst()) {
            unread = cursor.getInt(0);
        }
        db.close();
        return unread;
    }

    public void exportDatabse(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + "com.dd.menyoo" + "//databases//" + databaseName + "";
                String backupDBPath = "backupname.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();

                }
                Log.e("MEnyoo", "Exported easily");
            }
        } catch (Exception e) {

        }
    }

}
