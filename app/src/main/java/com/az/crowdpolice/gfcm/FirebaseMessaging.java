package com.az.crowdpolice.gfcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;


import com.az.crowdpolice.MainActivity;
import com.az.crowdpolice.R;
import com.az.crowdpolice.bussiness.ChatBusiness;
import com.az.crowdpolice.bussiness.SessionClass;
import com.az.crowdpolice.helper.NetworkHelper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

/**
 * Created by Administrator on 11/29/2016.
 */

public class FirebaseMessaging extends FirebaseMessagingService {

    private String from;
    private String message;
    public static String FROM_NOTIFICATION = "isFromNotification";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        this.from = remoteMessage.getFrom();

        if (remoteMessage.getData().size() > 0) {
            this.message = remoteMessage.getData().get("message");
            message = NetworkHelper.getDecodedString(message);
            sendNotification();
        }
        ChatBusiness chatBusiness = new ChatBusiness(SessionClass.getInstance().
                getCurrentActivity()==null?this:SessionClass.getInstance().getCurrentActivity());
        chatBusiness.getAllMessageFromServer();

        if (remoteMessage.getNotification() != null) {
            this.message = remoteMessage.getNotification().getBody();
            sendNotification();
        }


    }

    private void sendNotification() {
        Log.i("push","Notification Called");
        Random randomGenerator = new Random();

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(FROM_NOTIFICATION, true);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis() /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setSmallIcon(getNotificationIcon())
                .setContentText(message)
                .setContentTitle(getString(R.string.app_name))
                .setColor(ContextCompat.getColor(this, R.color.colorAccent))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        notificationBuilder.setVisibility(1);
        notificationBuilder.setPriority(Notification.PRIORITY_MAX);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int randomInt = randomGenerator.nextInt(100);
        notificationManager.notify(randomInt, notificationBuilder.build());
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }


}
