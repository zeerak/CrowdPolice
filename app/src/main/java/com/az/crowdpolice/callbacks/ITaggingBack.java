package com.az.crowdpolice.callbacks;

/**
 * Created by Zeera on 3/5/2017.
 */

public interface ITaggingBack {
    String actionBarActionName();
    int actionBarImage();
    void performAction();
}
