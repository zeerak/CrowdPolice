package com.az.crowdpolice.callbacks;

/**
 * Created by Zeera on 4/22/2017.
 */

public interface IMessageReceived {
    void onMessageReceived(Integer userId);
    void onGroupMessageReceived(Integer groupId);
}
