package com.az.crowdpolice.callbacks;

import android.view.View;

/**
 * Created by Administrator on 10/21/2016.
 */

public interface IItemClickListener {
    void onClick(View view, int pos);
}
