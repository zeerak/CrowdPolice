package com.az.crowdpolice.callbacks;

/**
 * Created by Zeera on 4/22/2017.
 */

public interface IKeyBoardOpen {
    void keyBoardStateChanged(boolean isOpen);
}
