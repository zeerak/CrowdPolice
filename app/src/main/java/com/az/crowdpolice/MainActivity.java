package com.az.crowdpolice;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.az.crowdpolice.bussiness.ActionBarHelper;
import com.az.crowdpolice.bussiness.ChatBusiness;
import com.az.crowdpolice.bussiness.SessionClass;
import com.az.crowdpolice.bussiness.ViewPagerBusiness;
import com.az.crowdpolice.callbacks.IKeyBoardOpen;
import com.az.crowdpolice.callbacks.ILocationUpdated;
import com.az.crowdpolice.callbacks.IMessageReceived;
import com.az.crowdpolice.callbacks.INetworkCallBack;
import com.az.crowdpolice.callbacks.ITaggingBack;
import com.az.crowdpolice.fragments.BaseFragment;
import com.az.crowdpolice.fragments.Entrance_fragment;
import com.az.crowdpolice.gfcm.FirebaseToken;
import com.az.crowdpolice.helper.GpsHelper;
import com.az.crowdpolice.helper.KeyBoardHelper;
import com.az.crowdpolice.model.BreakingNewsModel;
import com.az.crowdpolice.network.ApiImplementor;
import com.az.crowdpolice.network.ApiResponceParser;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.w3c.dom.Text;

import java.awt.font.NumericShaper;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements IMessageReceived, IKeyBoardOpen, ILocationUpdated {

    private static final String TAG = " CrowdPolice";
    private ChatBusiness mChatBussiness;

    @BindView(R.id.tl_main_tabs)
    TabLayout mTabLayout;
    @BindView(R.id.vp_main)
    ViewPager mViewPager;
    @BindView(R.id.tv_main_breakingnews)
    TextView mtvBreakingNews;
    @BindView(R.id.ib_main_back)
    ImageButton mActionBack;
    @BindView(R.id.tv_actionBar_title)
    TextView mActionBarTitle;
    @BindView(R.id.ib_main_slider)
    ImageButton mDrawerOpen;
    @BindView(R.id.llback)
    LinearLayout mLlBack;
    @BindView(R.id.tv_main_back)
    TextView mTvBack;
    @BindView(R.id.tv_actionBar_action)
    TextView mTvAction;

    private GpsHelper mGpsHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        postUserToken();
        setActionBarAndFragment();
        new KeyBoardHelper().setListenerToRootView(this, this);
        mGpsHelper = new GpsHelper(this);
        requestLocation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBreakNews();
        getAllMessage();

    }

    private void setActionBarAndFragment() {
        new ActionBarHelper(this);
        new ViewPagerBusiness(mTabLayout, mViewPager, this, getSupportFragmentManager(),
                ViewPagerBusiness.TYPE_PAGER.CRIME);

        mFragmentManager = getSupportFragmentManager();
        mContainerID = R.id.fl_fragmentContainer;
        setBackStackListener();
        replaceFragment(new Entrance_fragment(), false);
    }

    private void getBreakNews() {
        ApiImplementor.getInstance().getBreakingNews(new INetworkCallBack() {
            @Override
            public void onSuccess(String response) {
                try {
                    ArrayList<BreakingNewsModel> news = ApiResponceParser
                            .getInstance()
                            .getBreakingNews(response);
                    String breakingNews = "";
                    for (BreakingNewsModel aNew : news) {
                        if (breakingNews.length() == 0)
                            breakingNews = aNew.getNews();
                        else
                            breakingNews += "  " + aNew.getNews();
                    }
                    mtvBreakingNews.setText(Html.fromHtml(breakingNews));
                    mtvBreakingNews.setVisibility(View.VISIBLE);
                    mtvBreakingNews.setSelected(true);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void onFragmentUpdate(BaseFragment fragment) {
        if (fragment != null && fragment instanceof ITaggingBack) {
            mDrawerOpen.setVisibility(View.GONE);
            mLlBack.setVisibility(View.VISIBLE);
            mTvAction.setText(((ITaggingBack) fragment).actionBarActionName());

            mActionBarTitle.setVisibility(View.GONE);
            if (!fragment.getName().isEmpty())
                mTvBack.setText(((ITaggingBack) fragment).actionBarActionName());
            else
                mTvBack.setText("Back");
        } else {
            mDrawerOpen.setVisibility(View.VISIBLE);
            mLlBack.setVisibility(View.GONE);
            mActionBarTitle.setVisibility(View.VISIBLE);
            mTvAction.setText("");
        }
    }

    @OnClick(R.id.tv_actionBar_action)
    public void perfromFragmentAction(){
        if(getCurrentFragment()!=null&& getCurrentFragment()  instanceof ITaggingBack){
            ((ITaggingBack) getCurrentFragment()).performAction();
        }
    }

    @OnClick(R.id.llback)
    public void popFragment() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onMessageReceived(@Nullable Integer userId) {
        if (getCurrentFragment() instanceof IMessageReceived) {
            ((IMessageReceived) getCurrentFragment()).onMessageReceived(userId);
        }
    }

    @Override
    public void onGroupMessageReceived(@Nullable Integer groupId) {
        if (getCurrentFragment() instanceof IMessageReceived)
            ((IMessageReceived) getCurrentFragment()).onGroupMessageReceived(groupId);
    }

    public void getAllMessage() {
        if (mChatBussiness == null)
            mChatBussiness = new ChatBusiness(this);
        mChatBussiness.getAllMessageFromServer();
    }

    private void postUserToken() {
        String fcmToken;
        if (FirebaseInstanceId.getInstance().getToken() != null) {
            fcmToken = FirebaseInstanceId.getInstance().getToken();
        } else {

            fcmToken = "";
        }
        ApiImplementor.getInstance().updateToken(SessionClass.getInstance().getUserId()
                , fcmToken);
    }

    @Override
    public void keyBoardStateChanged(boolean isOpen) {
        if (isOpen) {
            mtvBreakingNews.setSelected(true);
        }
        if (getCurrentFragment() instanceof IKeyBoardOpen) {
            ((IKeyBoardOpen) getCurrentFragment()).keyBoardStateChanged(isOpen);
        }
    }

    public void getGroupMessages(int groupId) {
        if (mChatBussiness != null)
            mChatBussiness.getAllMessagesOfGroup(groupId);
    }

    public void getUserMessages(int userid) {
        if (mChatBussiness != null)
            mChatBussiness.getMessagesFromServer(userid);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == GpsHelper.REQUEST_GPS && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            requestLocation();
        }
    }

    public void requestLocation() {
        if (mGpsHelper != null)
            mGpsHelper.askForPermmision(this);
    }

    @Override
    public void onUpdate(Location location) {
        if (getCurrentFragment() instanceof ILocationUpdated)
            ((ILocationUpdated) getCurrentFragment()).onUpdate(location);
    }
}
