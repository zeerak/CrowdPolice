package com.az.crowdpolice;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.az.crowdpolice.adapter.LocationAdapter;
import com.az.crowdpolice.callbacks.IItemClickListener;
import com.az.crowdpolice.helper.GpsHelper;
import com.az.crowdpolice.model.PlaceAutoCompleteModel;
import com.az.crowdpolice.model.PlaceObject;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.SphericalUtil;

import java.io.Serializable;

public class LocationActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    public static final String PLACE_KEY = "keyPlace";
    private GoogleApiClient mGoogleApiClient;
    private LocationAdapter mLocationAdpter;
    private EditText mSearch;
    private RecyclerView mRvLocations,mRvRecentLocation;
    private LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        mGoogleApiClient = new GoogleApiClient.Builder(LocationActivity.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, this)
                .addConnectionCallbacks(this)
                .build();
        mSearch = (EditText) findViewById(R.id.et_search);
        mRvLocations = (RecyclerView) findViewById(R.id.rv_location);

        setAdapter();
    }

    private void setAdapter() {

        mLocationAdpter = new LocationAdapter(this, BOUNDS_MOUNTAIN_VIEW, null);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        mRvLocations.setLayoutManager(layoutManager);
        mLocationAdpter.setGoogleApiClient(mGoogleApiClient);
        mLocationAdpter.setmListener(listener);
        mRvLocations.setAdapter(mLocationAdpter);
        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mLocationAdpter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private IItemClickListener listener = new IItemClickListener() {
        @Override
        public void onClick(View view, int pos) {
            final PlaceAutoCompleteModel item = mLocationAdpter.getItem(pos);
            final String placeId = String.valueOf(item.placeId);
            getPlace(placeId);
        }
    };

    private void getPlace(String placeId) {
        // Log.i(LOG_TAG, "Selected: " + item.primaryLocation);
        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                .getPlaceById(mGoogleApiClient, placeId);
        placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
        // Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
    }

    private String LOG_TAG="croowdpolice";
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            // TODO: 4/13/2017 add setter here
            //AppController.setPlace(place);
            Intent intent = new Intent();
            PlaceObject placeObject = new PlaceObject(place.getName().toString(),
                    place.getLatLng().latitude,place.getLatLng().longitude,place.getAddress().toString());
            intent.putExtra(PLACE_KEY, placeObject);
            setResult(RESULT_OK,intent);
            finish();
            CharSequence attributions = places.getAttributions();
        }
    };

    @Override
    public void onConnected(Bundle bundle) {
        mLocationAdpter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());
        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mLocationAdpter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    public static LatLngBounds latitudeLongitudeToBounds(LatLng center, double radius) {
        LatLng southwest = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 225);
        LatLng northeast = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 45);
        return new LatLngBounds(southwest, northeast);
    }
}
