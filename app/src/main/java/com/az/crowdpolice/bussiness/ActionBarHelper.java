package com.az.crowdpolice.bussiness;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;

import com.az.crowdpolice.BaseActivity;
import com.az.crowdpolice.R;
import com.az.crowdpolice.adapter.DrawerAdapter;
import com.az.crowdpolice.callbacks.IItemClickListener;
import com.az.crowdpolice.fragments.ChatRootFragment;
import com.az.crowdpolice.fragments.CrimeMapFragment;
import com.az.crowdpolice.fragments.CrimeTipsHostFragment;
import com.az.crowdpolice.fragments.EmergencyLocator;
import com.az.crowdpolice.fragments.LostFoundItem;
import com.az.crowdpolice.model.MenuItem;

import java.util.ArrayList;


/**
 * Created by Zeera on 3/4/2017.
 */

public class ActionBarHelper implements View.OnClickListener,IItemClickListener {

    private ImageButton menuicon;
    private ImageButton callicon;
    private DrawerLayout drawerLayout;
    private ArrayList<MenuItem> menuItems;
    private RecyclerView recyclerView;
    private DrawerAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ActionBarDrawerToggle mDrawerToggle;
    private Activity activity;


    public ActionBarHelper(Activity activity) {
        this.activity = activity;
        menuItems = new ArrayList<>();
        setUpActionBar();
    }

    private void setUpActionBar() {
        setMenuItems();
        initializeView();
        setDrawerToggle();
    }

    public void addMenuItem(MenuItem item) {
        menuItems.add(item);
    }

    private void setMenuItems(){
        menuItems.add(new MenuItem(R.string.drawer_home,R.drawable.home_icon,R.drawable.home_icon,null));
        menuItems.add(new MenuItem(R.string.drawer_Login,R.drawable.settings_icon,R.drawable.settings_icon,null));
        menuItems.add(new MenuItem(R.string.drawer_Chatroom, R.drawable.settings_icon, R.drawable.settings_icon, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseActivity)activity).replaceFragment(new ChatRootFragment(),true);
            }
        }));
        menuItems.add(new MenuItem(R.string.drawer_alert, R.drawable.settings_icon, R.drawable.settings_icon, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseActivity)activity).replaceFragment(new EmergencyLocator(),true);
            }
        }));
        menuItems.add(new MenuItem(R.string.drawer_assert,R.drawable.settings_icon,R.drawable.settings_icon,null));
        menuItems.add(new MenuItem(R.string.drawer_tips, R.drawable.settings_icon, R.drawable.settings_icon, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseActivity)activity).replaceFragment(new CrimeTipsHostFragment(),true);
            }
        }));
        menuItems.add(new MenuItem(R.string.drawer_map, R.drawable.settings_icon, R.drawable.settings_icon, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseActivity)activity).replaceFragment(new CrimeMapFragment(),true);
            }
        }));
        menuItems.add(new MenuItem(R.string.drawer_news,R.drawable.settings_icon,R.drawable.settings_icon,null));
        menuItems.add(new MenuItem(R.string.drawer_locator,R.drawable.settings_icon,R.drawable.settings_icon,null));
        menuItems.add(new MenuItem(R.string.drawer_services,R.drawable.settings_icon,R.drawable.settings_icon,null));
        menuItems.add(new MenuItem(R.string.drawer_topics,R.drawable.settings_icon,R.drawable.settings_icon,null));
        menuItems.add(new MenuItem(R.string.drawer_lostfound,R.drawable.settings_icon,R.drawable.settings_icon, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseActivity)activity).replaceFragment(new LostFoundItem(),true);
            }
        }));
        menuItems.add(new MenuItem(R.string.drawer_shopping,R.drawable.settings_icon,R.drawable.settings_icon,null));
        menuItems.add(new MenuItem(R.string.drawer_help,R.drawable.settings_icon,R.drawable.settings_icon,null));
        menuItems.add(new MenuItem(R.string.drawer_setting,R.drawable.settings_icon,R.drawable.settings_icon,null));
        menuItems.add(new MenuItem(R.string.drawer_exit,R.drawable.exit,R.drawable.exit, new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                activity.finishAffinity();
            }
        }));
    }


    private void initializeView() {
        recyclerView = (RecyclerView) activity.findViewById(R.id.rv_drawer_main);
        menuicon = (ImageButton) activity.findViewById(R.id.ib_main_slider);
        menuicon.setOnClickListener(this);
        drawerLayout = (DrawerLayout) activity.findViewById(R.id.activity_drawer);
        setAdapter();
    }

    private void setAdapter() {
        adapter = new DrawerAdapter(menuItems, activity, this);
        layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void setDrawerToggle() {
        mDrawerToggle = new ActionBarDrawerToggle(activity, drawerLayout,
                R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public boolean onOptionsItemSelected(android.view.MenuItem item) {
                if (item != null && item.getItemId() == android.R.id.home) {
                    if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                    } else {
                        drawerLayout.openDrawer(Gravity.LEFT);
                    }
                }
                return false;
            }


        };
        drawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ib_main_slider:
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
                break;
        }
    }


    @Override
    public void onClick(View view, int pos) {

        int clickItemID = menuItems.get(pos).getNameId();
        drawerLayout.closeDrawer(Gravity.LEFT);
        if(menuItems.get(pos).getListener()!=null)
            menuItems.get(pos).getListener().onClick(view);

    }

    }

/*    private void toSignInActivity() {
            Intent i = new Intent(activity, SignIn.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(i);
    }*/


