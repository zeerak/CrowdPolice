package com.az.crowdpolice.bussiness;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.az.crowdpolice.adapter.ViewPagerAdapter;
import com.az.crowdpolice.fragments.BaseFragment;
import com.az.crowdpolice.fragments.CrimeSpy;
import com.az.crowdpolice.fragments.Emergency;
import com.az.crowdpolice.fragments.Status;
import com.az.crowdpolice.fragments.Wanted;
import com.az.crowdpolice.model.PagerModel;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Zeera on 3/5/2017.
 */

public class ViewPagerBusiness {
    private TabLayout tabLayout;
    private ViewPager pager;
    private Context mContext;
    private FragmentManager fm;
    private BaseFragment mCurrentFragment;


    public enum TYPE_PAGER {
        CRIME,HOME
    }


    private ArrayList<PagerModel> getFragments() {
        ArrayList<PagerModel> arrayList = new ArrayList<>();
        arrayList.add(new PagerModel("Emergency", Emergency.newInstance()));
        arrayList.add(new PagerModel("Status", Status.newInstance()));
        return arrayList;
    }

    private ArrayList<PagerModel> getFragmentsCrime() {
        ArrayList<PagerModel> arrayList = new ArrayList<>();
        arrayList.add(new PagerModel("CrimeSpy", CrimeSpy.newInstance()));
        arrayList.add(new PagerModel("Wanted", Wanted.newInstance()));
        return arrayList;
    }

    public ViewPagerBusiness(TabLayout tabLayout, ViewPager pager,
                             Context mContext, FragmentManager fm, TYPE_PAGER type) {
        this.tabLayout = tabLayout;
        this.pager = pager;
        this.mContext = mContext;
        this.fm = fm;

        setAdapter(type );
    }

    private void setAdapter(TYPE_PAGER type){
        ArrayList<PagerModel> data = null;
        switch (type){
            case HOME:
                data = getFragments();
                break;
            case CRIME:
                data = getFragmentsCrime();
                break;
        }
        if(data==null)
            return;
        final ViewPagerAdapter adapter = new ViewPagerAdapter(fm,data);

        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager);
        mCurrentFragment =adapter.getItem(0);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mCurrentFragment = adapter.getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Nullable
    public BaseFragment getmCurrentFragment() {
        return mCurrentFragment;
    }
}
