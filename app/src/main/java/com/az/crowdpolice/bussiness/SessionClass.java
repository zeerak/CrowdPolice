package com.az.crowdpolice.bussiness;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.support.annotation.Nullable;

import com.az.crowdpolice.database.DataBasehadlerClass;
import com.az.crowdpolice.helper.LogHelper;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Zeera on 4/15/2017.
 */

public class SessionClass {

    private static SessionClass mInstance;
    private int userId =3;
    private Activity currentActivity;
    private DataBasehadlerClass mDbHelper;
    private Location mCurrentUserLocation;

    private SessionClass() {
        //no instance
    }

    public static SessionClass getInstance() {
        if(mInstance==null)
            mInstance = new SessionClass();
        return mInstance;
    }


    public int getUserId() {
        return userId;
    }

    public DataBasehadlerClass getmDbHelper(Context context) {
        if(mDbHelper==null)
            mDbHelper = new DataBasehadlerClass(context);
        return mDbHelper;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
        LogHelper.debugLog(currentActivity.getClass().getSimpleName());
    }

    public void refreshActivity(){
        LogHelper.debugLog("Activity is set to null ");
        this.currentActivity = null;
    }

    @Nullable
    public Activity getCurrentActivity() {
        return currentActivity;
    }

    @Nullable
    public Location getmCurrentUserLocation() {
        return mCurrentUserLocation;
    }

    public void setmCurrentUserLocation(Location mCurrentUserLocation) {
        this.mCurrentUserLocation = mCurrentUserLocation;
    }
}
