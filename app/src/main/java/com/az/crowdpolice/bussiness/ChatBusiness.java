package com.az.crowdpolice.bussiness;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.az.crowdpolice.callbacks.IMessageReceived;
import com.az.crowdpolice.callbacks.INetworkCallBack;
import com.az.crowdpolice.database.DataBasehadlerClass;
import com.az.crowdpolice.helper.DateHelper;
import com.az.crowdpolice.model.MessageModel;
import com.az.crowdpolice.network.ApiClient;
import com.az.crowdpolice.network.ApiImplementor;
import com.az.crowdpolice.network.ApiResponceParser;
import com.az.crowdpolice.network.IApiInterface;

import org.json.JSONException;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by Zeera on 4/20/2017.
 */

public class ChatBusiness {
    private Context mCtx;

    public ChatBusiness(Context mCtx) {
        this.mCtx = mCtx;
    }

    public void getMessagesFromServer(final Integer userId) {
        Integer lastMessageId = SessionClass.getInstance().getmDbHelper(mCtx).getLastIdOfChat(false);
        ApiImplementor.getInstance().getMessages(new INetworkCallBack() {
            @Override
            public void onSuccess(String responce) {
                try {
                    addMessageToDb(ApiResponceParser.getInstance().getMessages(responce),userId,false);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, userId, lastMessageId);
    }


    public void getAllMessageFromServer() {
        Integer lastMessageId = SessionClass.getInstance().getmDbHelper(mCtx).getLastIdOfChat(false);
        ApiImplementor.getInstance().getAllMesageFromServer(new INetworkCallBack() {
            @Override
            public void onSuccess(String response) {
                try {
                    addMessageToDb(ApiResponceParser.getInstance().getMessages(response),null,false);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },SessionClass.getInstance().getUserId(), lastMessageId);
    }

    private void addMessageToDb(ArrayList<MessageModel> messageModels, Integer userId,boolean isGr) {
        DataBasehadlerClass dp = SessionClass.getInstance().getmDbHelper(mCtx);
        for (MessageModel messageModel : messageModels) {
            messageModel.setRead(false);
            messageModel.setSendingTime(DateHelper.getDateForDB(messageModel.getSendingTime()));
            dp.addMessage(messageModel,isGr);
        }
        updateOneToOne(userId);
    }

    public void getAllMessagesOfGroup(Integer groupId){
        Integer lastMessageId = SessionClass.getInstance().getmDbHelper(mCtx).getLastIdOfChat(true);
        ApiImplementor.getInstance().getGroupMessageFromServer(new INetworkCallBack() {
            @Override
            public void onSuccess(String response) {
                try {
                    addMessageToDb(ApiResponceParser.getInstance().getMessages(response),null,true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },groupId, lastMessageId,SessionClass.getInstance().getUserId());
    }

    private void updateOneToOne(@Nullable Integer userId){
        if (mCtx instanceof IMessageReceived) {
            ((IMessageReceived) mCtx).onMessageReceived(userId);
        }
    }
}
