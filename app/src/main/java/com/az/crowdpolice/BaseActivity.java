package com.az.crowdpolice;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.az.crowdpolice.bussiness.SessionClass;
import com.az.crowdpolice.fragments.BaseFragment;

/**
 * Created by Zeera on 3/5/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    FragmentManager mFragmentManager;
    int mContainerID;

    /**
     * replace current fragment with new passed fragment and update the title in actionBar as well
     *
     * @param fragment         the new fragment.
     * @param isToAddBackStack true in case you want to add item to back stack else false
     */
    public void replaceFragment(BaseFragment fragment, boolean isToAddBackStack) {
        if (mFragmentManager == null) {
            //setFragmentView();

        }
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(mContainerID, fragment, "KK");
        if (isToAddBackStack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
        onFragmentUpdate(fragment);
    }

    public BaseFragment getCurrentFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(mContainerID);
        if (fragment instanceof BaseFragment)
            return ((BaseFragment) fragment);
        return null;
    }

    protected void setBackStackListener() {
        mFragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Fragment fr = getSupportFragmentManager().findFragmentById(mContainerID);

                if (fr != null) {
                    if (fr instanceof BaseFragment) {
                        onFragmentUpdate((BaseFragment) fr);
                    }
                } else {
                    onFragmentUpdate(null);
                }
            }
        });
    }

    public abstract void onFragmentUpdate(BaseFragment fragment);

    public void popStack() {
        if (mFragmentManager != null)
            mFragmentManager.popBackStack();
    }

    @Override
    public void onBackPressed() {
        if (getCurrentFragment() == null || !getCurrentFragment().performBackFragment())
            super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SessionClass.getInstance().setCurrentActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SessionClass.getInstance()
                .refreshActivity();
    }
}
