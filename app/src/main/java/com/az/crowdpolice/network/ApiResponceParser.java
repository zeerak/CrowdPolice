package com.az.crowdpolice.network;

import com.az.crowdpolice.model.BreakingNewsModel;
import com.az.crowdpolice.model.CrimeCategories;
import com.az.crowdpolice.model.CrimeModel;
import com.az.crowdpolice.model.GroupModel;
import com.az.crowdpolice.model.MessageModel;
import com.az.crowdpolice.model.UserModel;
import com.az.crowdpolice.model.WantedModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Zeera on 3/5/2017.
 */

public class ApiResponceParser  {

    private static  ApiResponceParser mInstance ;
    private ApiResponceParser() {
        //no instance
    }

    public static ApiResponceParser getInstance() {
        if(mInstance==null)
            mInstance = new ApiResponceParser();
        return mInstance;
    }

    public ArrayList<BreakingNewsModel> getBreakingNews(String jsonResponce) throws JSONException{
        ArrayList<BreakingNewsModel> breakingNews = new ArrayList<>();
        JSONArray responce = new JSONArray(jsonResponce);
        Gson gson = new GsonBuilder().create();
        for(int idx=0;idx<responce.length();idx++){
            breakingNews.add(gson.fromJson(responce.getJSONObject(idx).toString(),BreakingNewsModel.class));
        }
        return breakingNews;
    }

    public ArrayList<CrimeCategories> getCrimeCategories(String jsonResponce) throws JSONException{
        ArrayList<CrimeCategories> categories = new ArrayList<>();
        JSONArray jArr = new JSONArray(jsonResponce);
        Gson gson = new GsonBuilder().create();
        for (int i = 0; i < jArr.length(); i++) {
            categories.add(gson.fromJson(jArr.get(i).toString(),CrimeCategories.class));
        }
        return categories;
    }

    public ArrayList<WantedModel> getWanted(String jsonResponce) throws JSONException{
        ArrayList<WantedModel> criminals = new ArrayList<>();
        JSONArray jArr = new JSONArray(jsonResponce);
        Gson gson = new GsonBuilder().create();
        for (int i = 0; i < jArr.length(); i++) {
            criminals.add(gson.fromJson(jArr.get(i).toString(),WantedModel.class));
        }
        return criminals;
    }

    public ArrayList<CrimeModel> getCrimes(String jsonres) throws JSONException{
        ArrayList<CrimeModel> crimes = new ArrayList<>();
        JSONObject jRes = new JSONObject(jsonres);
        JSONArray jCrimes = jRes.getJSONArray("ReportCrimeList");
        for (int i = 0; i < jCrimes.length(); i++) {
            Gson gson = new GsonBuilder().create();
            CrimeModel crime = gson.fromJson(jCrimes.get(i).toString(),CrimeModel.class);
            crimes.add(crime);
        }
        return crimes;
    }

    public ArrayList<UserModel> getUsers(String json) throws JSONException{
        ArrayList<UserModel> users = new ArrayList<>();
        JSONArray jUsers = new JSONArray(json);
        for (int i = 0; i < jUsers.length(); i++) {
            Gson gson = new GsonBuilder().create();
            UserModel user = gson.fromJson(jUsers.get(i).toString(),UserModel.class);
            users.add(user);

        }
        return users;
    }

    public String getStatistic(String json) throws JSONException{
        return new JSONObject(json).getJSONObject("CrimeStatistics").toString();
    }

    public ArrayList<GroupModel> getGroups(String json) throws JSONException{
        ArrayList<GroupModel> groups = new ArrayList<>();
        JSONArray jUsers = new JSONArray(json);
        for (int i = 0; i < jUsers.length(); i++) {
            Gson gson = new GsonBuilder().create();
            GroupModel group = gson.fromJson(jUsers.get(i).toString(),GroupModel.class);
            groups.add(group);

        }
        return groups;
    }

    public ArrayList<MessageModel> getMessages(String json)throws JSONException{
        ArrayList<MessageModel> messages = new ArrayList<>();
        JSONArray jUsers = new JSONArray(json);
        for (int i = 0; i < jUsers.length(); i++) {
            Gson gson = new GsonBuilder().create();
            MessageModel message = gson.fromJson(jUsers.get(i).toString(),MessageModel.class);
            messages.add(message);

        }
        return messages;
    }
}
