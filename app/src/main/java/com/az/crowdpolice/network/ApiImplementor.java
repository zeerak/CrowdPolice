package com.az.crowdpolice.network;

import android.support.annotation.Nullable;
import android.util.Log;

import com.az.crowdpolice.callbacks.INetworkCallBack;
import com.az.crowdpolice.model.BreakingNewsModel;
import com.az.crowdpolice.model.CrimeModel;
import com.az.crowdpolice.model.MessageModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Zeera on 3/5/2017.
 */

public class ApiImplementor {
    private IApiInterface mApiclient;
    private static ApiImplementor mInstance;

    private ApiImplementor() {
        //no instance
        mApiclient = ApiClient.getClient().create(IApiInterface.class);
    }

    public static ApiImplementor getInstance() {
        if (mInstance == null)
            mInstance = new ApiImplementor();
        return mInstance;
    }


    public void getBreakingNews(final INetworkCallBack callback) {
        Call<ResponseBody> getBreakingNews = mApiclient.getBreakingNews();
        getBreakingNews.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    callback.onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    public void getCrimesCategories(final INetworkCallBack callBack) {
        Call<ResponseBody> getCategories = mApiclient.getCrimCategories();
        getCategories.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    callBack.onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void getWanted(final INetworkCallBack callBack) {
        Call<ResponseBody> getCategories = mApiclient.getWanted();
        getCategories.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    callBack.onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void getReportedCrimes(final INetworkCallBack callBack, @Nullable Double lat, @Nullable Double lng) {
        Call<ResponseBody> getreportedCrimes;
        if (lat == null || lng == null)
            getreportedCrimes = mApiclient.getReportedCrimes();
        else
            getreportedCrimes = mApiclient.getReportedCrimes(lat, lng);
        getreportedCrimes.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    callBack.onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void getAllUsers(final INetworkCallBack callBack) {
        Call<ResponseBody> getUser = mApiclient.getRegisteredusers();
        getUser.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    callBack.onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void getAllGroups(final INetworkCallBack callBack) {
        Call<ResponseBody> getUser = mApiclient.getGroups();
        getUser.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    callBack.onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void sendMessage(final INetworkCallBack callBack, MessageModel messageModel, boolean isGroup) {

        Call<ResponseBody> sendMessage;
        if (isGroup)
            sendMessage = mApiclient.sendGroupMessage(messageModel);
        else
            sendMessage = mApiclient.sendMessage(messageModel);
        sendMessage.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    callBack.onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void updateToken(int userId, String token) {
        Call<ResponseBody> updateToken = mApiclient.updateToken(userId, token);
        updateToken.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void getMessages(final INetworkCallBack callBack, int userId, @Nullable Integer lastMessageId) {
        Call<ResponseBody> getMessages;
        if (lastMessageId == null) {
            getMessages = mApiclient.getMessages(userId);
        } else
            getMessages = mApiclient.getMessages(userId, lastMessageId);

        getMessages.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    callBack.onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void getAllMesageFromServer(final INetworkCallBack callBack, int userId, @Nullable Integer lastMessageId) {
        Call<ResponseBody> getMessages;
        getMessages = mApiclient.getAllMesssage(userId, lastMessageId);
        getMessages.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    callBack.onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void getGroupMessageFromServer(final INetworkCallBack callBack,
                                          Integer groupId, @Nullable Integer senderId,
                                          @Nullable Integer lastMessageId) {
        Call<ResponseBody> getMessages;
        getMessages = mApiclient.getAllMessageOfGroup(groupId, lastMessageId, senderId);

        getMessages.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    callBack.onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void postCrime(CrimeModel crimeModel,final INetworkCallBack callBack){
        Call<ResponseBody> postCrime = mApiclient.postCrime(crimeModel);
        postCrime.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    callBack.onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
