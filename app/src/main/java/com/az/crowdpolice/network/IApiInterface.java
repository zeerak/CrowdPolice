package com.az.crowdpolice.network;
//Get pending bookings


import com.az.crowdpolice.model.CrimeModel;
import com.az.crowdpolice.model.MessageModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by Administrator on 9/16/2016.
 */
public interface IApiInterface {

    @GET("/api/BreakingNews/GetBreakingNews")
    Call<ResponseBody> getBreakingNews();

    @GET("/api/LostFound/GetLostFound")
    Call<ResponseBody> getLostFound();

    @GET("/api/LostFound/GetItemCategories")
    Call<ResponseBody> getLostFoundCategory();

    @GET("api/Wanted/GetCrimeCategories")
    Call<ResponseBody> getCrimCategories();

    @GET("api/Wanted/GetWanted")
    Call<ResponseBody> getWanted();

    @GET("api/ReportCrime/GetReportCrime")
    Call<ResponseBody> getReportedCrimes();

    @GET("api/ReportCrime/GetNearByLocationCrimes")
    Call<ResponseBody> getReportedCrimes(@Query("Latitude") double lat, @Query("Longitude") double lng);

    @GET("/api/Chat/GetRegisteredUsers")
    Call<ResponseBody> getRegisteredusers();

    @GET("/api/Chat/GetChatGroups")
    Call<ResponseBody> getGroups();

    @GET("/api/Chat/GetAllRecievedMessagesOfUser")
    Call<ResponseBody> getMessages(@Query("UserId") int userID);

    @GET("/api/Chat/GetLastRecievedMessagesOfUser")
    Call<ResponseBody> getMessages(@Query("UserId") int userID, @Query("LastChatId") int chatId);

    @GET("/api/Chat/GetAllMessagesOfUser")
    Call<ResponseBody> getAllMesssage(@Query("UserId") Integer userId, @Query("LastMessageId") Integer lastMessageId);

    @GET("/api/Chat/GetRecievedMessagesOfGroup")
    Call<ResponseBody> getAllMessageOfGroup(@Query("GroupId") int groupId,@Query("senderUserId")Integer senderId,
                                            @Query("LastChatId")Integer chatId);

    @POST("/api/ReportCrime/Post")
    Call<ResponseBody> postCrime(@Body CrimeModel crimeModel);

    @POST("/api/Chat/PostChatMessage")
    Call<ResponseBody> sendMessage(@Body() MessageModel message);

    @POST("/api/Chat/PostGroupChatMessage")
    Call<ResponseBody> sendGroupMessage(@Body MessageModel message);

    @PUT("/api/Users/UpdateUserDeviceToken")
    Call<ResponseBody> updateToken(@Query("Id") int userId, @Query("DeviceToken") String deviceToken);


    //send sms to passenger from sma gateway
    @FormUrlEncoded
    @POST("servicehandler.ashx")
    Call<ResponseBody> sendSmsToPassenger(@Field("action") String action,
                                          @Field("passengerId") int passengerId,
                                          @Field("message") String message);

    //Get Map Points(Can only be called using map client)
    @GET("directions/json")
    Call<ResponseBody> getMapsPoints(@Query("origin") String origin, @Query("destination")
            String destination, @Query("sensor") boolean isSensor);


}
